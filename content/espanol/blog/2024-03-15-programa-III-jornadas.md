---
title: Contenidos de las III Jornadas de Cultura Libre
date: 2024-03-14
description: "Resumen  del programa y los ponentes que participarán en las III Jornadas"
slug: programa-III-jornadas
type: post
categories:
  - OfiLibre
tags:
  - "Jornadas"
  - "URJC"
  - "Cultura libre"

bg_image: /images/CONTENIDO_III_JORNADAS_bg.png
thumb: /images/Contenido_III_Jornadas_thumb.png
---

Programa de las Jornadas, incluyendo enlaces a los materiales utilizados en las presentaciones y talleres. Los pósters los puedes encontrar en [esta entrada del blog.](../pósters-de-las-iii-jornadas-de-cultura-libre)

## Miércoles 20 de marzo

##### ACTO DE APERTURA - CAFÉ CON LA OFILIBRE

Como acto de apertura tenemos uno de nuestros [cafés con la OfiLibre](/acciones/cafes/) en directo, con la colaboración de Mercedes del Hoyo (Vicerrectora de Comunidad Campus, Cultura y Deporte)

##### PRESENTACIONES

* Museo Virtual de la URJC en el Metaverso. Ponente y autor: Agustín Martín Peláez. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-02-01-Museo_Virtual.pdf))

* Clínica Jurídica URJC. Ponente y autor: David Belaguer Medrano.([Presentacion](/transpas/jornada-cultura-libre/2024/01-02-02-Clinica_Juridica.pdf))

##### PONENCIA INVITADA

* *Del Acceso  Abierto a la Ciencia Abierta: Retos de la Edición Técnica.* Autor y ponente: Remedios Pérez García, Jefa del Servicio de publicaciones Universidad Politécnica de Valencia ([Presentacion](/transpas/jornada-cultura-libre/2024/01-08-invitada-Reme_P_URJ.pdf))

##### PONENCIAS CORTAS

###### Wikimedia como paradigma de la cultura libre
* *Investigación científica e innovación docente a través de la cultura libre: El caso de la editatona #GastrónomasBNE.* Yanet Acosta. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-03-wiki-01-editatona_GastrónomasBNE.pdf))

* *Club Wikipedia URJC: Conocimiento abierto para mejorar la divulgación científica y el aprendizaje.* José María García de Madariaga. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-03-wiki-02_Club_wikipedia.pdf))

* *Wikitodología: Wikipedia como recurso de innovación docente en el Grado de Periodismo.* Rebeca Suárez. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-03-wiki-03WIKIPEDIA_03_SUÁREZ_ÁLVAREZ.pdf))

* *Wikipedia como herramienta para el uso y difusión de la producción científica.* Pilar de la Prieta (Wikimedia España). ([Presentacion](/transpas/jornada-cultura-libre/2024/01-03-wiki-04_Wikimedia.pdf))
###### Recursos educativos en abierto y cultura libre en las aulas

* *ECO2. Creación de un ecosistema de entornos de Conocimiento Abierto desde la innovación educativa.* María del Carmen Gálvez. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-05-rea-01-ECO2-Galvez.pdf))

* *Hemos creado los contenidos y ahora qué: el caso de los materiales del Grupo CINTER.* Félix Labrador. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-05-rea-02REA_02_LABRADOR_ARROYO.pdf))

* *Proyecto VIRION: Recursos Educativos Abiertos en realidad virtual para el fomento de las vocaciones STEM.* David García Marín.([Presentacion](/transpas/jornada-cultura-libre/2024/01-05-rea-03-Virion-david_martin.pdf))

* *Proyecto DOMINOES: resiliencia digital frente a la desinformación a través de la educación abierta.* Cristina Arribas. ([Presentacion](/transpas/jornada-cultura-libre/2024/01-05-rea-04-Dominoes.pdf)) 

* *Sostenibilidad, Recursos en Abierto y Software Libre.* Iria Paz.([Presentacion](/transpas/jornada-cultura-libre/2024/01-05-rea-05_REA_05_PAZ_GIL.pdf))

##### CHARLAS

* Innovación Educativa - CIED

* Sexenios. ¿Cómo fue el proceso de depósito y publicación en abierto en BURJC Digital?. Ponente: Fernando Silva de Biblioteca.([Presentacion](/transpas/jornada-cultura-libre/2024/01-07-sex-BURJC_SEXENIOS.pdf))

* Servicio de publicaciones de URJC. Presentación del Servicio de Revistas y Monografías en abierto. Ponente: Laura de la Cruz Parra

##### MESA REDONDA.

###### Logros y retos de las revistas URJC de acceso abierto: Un diálogo compartido con la comunidad científica. Moderado por Tomás Zarza (ASRI).

Colaboradores: 
* GUERRA COLONIAL (Miguel Madueño)

* NEOMEDIEVAL (Antonio Huertas)

* STUDIA HUMANITATIS JOURNAL (Roberto Barbeito y Sara Núñez de Prado)

* ORDEN INTERNACIONAL (José Manuel Azcona)

* LOAR (Begoña García)

* ICONO 14 (Manuel Gertrudix)

* FORUM DOCENTIS

##### TEATRO LIBRE LEÍDO: ANA YA NO TOCA EL CLARINETE, POR ARROJO ESCÉNICO URJC

##### TALLERES

* *OBS. REALIZACIÓN REMOTA EN DIRECTO*. Juan José Domínguez (URJC). 

* *DERECHOS DE AUTOR EN DOCENCIA E INVESTIGACIÓN: casos prácticos de la publicación abierta*. Emilio Alvarado (Biblioteca URJC). 

* *INTELIGENCIA ARTIFICIAL LIBRE: Introducción y práctica.* Asociación de inteligencia artificial (URJC).

* *TALLER DE SOFTWARE LIBRE.* Jesús M. González Barahona (URJC)

## Jueves 21 de marzo

#####  PONENCIA INVITADA

* **Datos en Abierto: Movimiento de Expansión**. Autor y Ponente: Sonia Castro García-Muñoz, Coordinadora en Red.es de la Iniciativa Aporta/datos.gob.es. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-04-invitada-DatosAbiertos_Castro_García-Muñoz.pdf))

##### PONENCIAS CORTAS
###### Datos en Abierto

* *Una base de datos de acceso público para analizar la representación de la infancia en la publicidad española.* Juan Manuel Vara Mesa. ([Presentacion](/transpas/jornada-cultura-libre/2024/))


* *Modelo emancipatorio de gestión de conocimiento aplicado a la prevención de conflictos organizacionales.* Helena Nadal Sánchez y Cristina Del Prado Higuera. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-06-CA-01-Modelo_emancipatorio.pdf))

###### Ciencia Abierta

* *PODCAST Y CONOCIMIENTO LIBRE: UNA EXPERIENCIA.*  Pablo Acosta Gallo. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-06-CA-02_Pablo_Acosta_podcast_libre.pdf))

* *MOVEDUCA: un podcast en abierto sobre movimiento y salud.* Francisco Molina Rueda ([Presentacion](/transpas/jornada-cultura-libre/2024/02-06-CA-03-Moveduca_Francisco_Molina_Rueda.pdf))

* *Estrategia de RRHH para investigadores: herramientas en abierto ¿Cómo puedes beneficiarte?.* Rosa Mesa Vélez. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-06-CA-04-CIENCIA_ABIERTA_03_MESA_VÉLEZ.pdf))

* *Reducción de errores durante la unificación de rúbricas de distintos Profesores de una misma asignatura en Ciencias de la Salud con el software libre R.* José Luis Arias Buría. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-06-CA-05-Reduccion_de_errores.pdf))

###### Software y Hardware Libre

* *Pasado, presente y futuro de EducaMadrid.* Adolfo Sanz de Diego.([Presentacion](/transpas/jornada-cultura-libre/2024/02-01-soft-01-Educamadrid.pdf))

* *JULIA, o cuando un programa libre es mejor que uno de pago.* Ana Isabel Muñoz Montalvo.([Presentacion](/transpas/jornada-cultura-libre/2024/02-01-soft-02-SHLIBRE_03_MUÑOZ_MONTALVO.pdf))

* *Acelerando el Diseño Hardware con Hardware Libre.* Rubén Nieto Capuchino. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-01-soft-03_Acelerando_el_diseño_hardware.pdf))

* *Administrando los 600 equipos de laboratorios docentes de la EIF con software libre.* Antonio Gutiérrez. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-01-soft-04_Laboratorio_linux_Jornadas_cultura_libre.pdf))


##### CHARLAS

* *Digitalización: Modernizando nuestras aplicaciones: OpenShift Comercial vs Kubernetes Software Libre.* Imparte: Ana María García Márquez Del Prado.
De cómo desde el Vicerrectorado de Transformación y educación digital e Inteligencia Artificial se llevó a cabo la elección de un sistema para desplegar aplicaciones, comparando distintas soluciones,tanto comerciales como software libre, evaluando sus ventajas e inconvenientes. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-02-digi-digitalizacion_modernizando_aplicaciones.pdf))

* *Escuela de Doctorado.* Desbloqueando al conocimiento: Tesis Doctorales en Abierto. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-09-EID-_Escuela_int_de_doctorado.pdf))

* *Jardines Digitales, una propuesta para liberar la investigación científica NODO COMÚN.* Daniel Cotillas, comunicador social y Pedro Fernández de Castro, investigador predoctoral. ([Presentacion](https://nodocomun.org/expo-jardines-digitales/?transition=zoom))

* *Bibliometría Narrativa: El impulso de la publicación en abierto.* Roberto Negral de Biblioteca. ([Presentacion](/transpas/jornada-cultura-libre/2024/02-08-Bibliometria_Narrativa.pdf))

#### OfiLibre. ¿ Todavía no sabes qué hacemos?

##### TARDE DE INTELIGENCIA ARTIFICIAL

* **Podcast libre. Podcast e inteligencia artificial.** José Antonio Gelado. 
La IA generativa está suponiendo una nueva Revolución Industrial en la producción, distribución y experiencia de consumo de podcasts. Un recorrido por los orígenes del podcasting y su evolución hasta la incorporación de la Inteligencia artificial en las distintas fases de ideación, grabación, edición y distribución y promoción. Para terminar con una mirada al futuro con cinco tendencias que experimentará su uso en el podcasting.

* **Wikimedia research en la era de la inteligencia artificial.** Pablo Aragón. 
Wikipedia y el resto de proyectos de Wikimedia son una pieza fundamental en el ecosistema del conocimiento libre. Los recientes avances en el campo de la Inteligencia Artificial ofrecen nuevos retos y oportunidades para el movimiento Wikimedia. En esta charla se abordará el uso actual de la Inteligencia Artificial por parte del equipo de investigación de la Fundación Wikimedia para contribuir a las estrategias de equidad del conocimiento y de conocimiento como servicio.

* **Inteligencia artificial generativa ABIERTA.** Hugging Face. 
La inteligencia artificial generativa ha tenido un desarrollo espectacular en los últimos meses. Además de modelos privados como ChatGPT, confinados en centros de datos de grandes empresas, existen también, afortunadamente, modelos abiertos como Llama o Stable Diffusion. Estos modelos han facilitado el crecimiento de un ecosistema abierto y colaborativo, donde la comunidad experimenta globalmente a un ritmo frenético, y es capaz de crear nuevos métodos, técnicas y aplicaciones. Hablaremos de los beneficios de Open Source ML, los desafíos que presenta y posibles tendencias de este vertiginoso mundo.

* **MESA DE DEBATE: INTELIGENCIA ARTIFICIAL ABIERTA**


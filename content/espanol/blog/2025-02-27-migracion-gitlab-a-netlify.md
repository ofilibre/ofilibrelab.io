---
title: "¡La OfiLibre está en Netlify!"
date: 2025-02-27
description: "A partir de este curso el sitio web de la OfiLibre está alojado en Netlify."
type: post
categories:
  - OfiLibre
tags:
  - "OfiLibre"

bg_image: /images/logo-ofilibre.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png
---

Nuestra web ya se encuentra alojada en Netlify. Si quieres saber cómo lo hemos hecho, en nuestra [última guía](../../guias/migracion-gitlab-a-netlify) lo hemos explicado detalladamente.
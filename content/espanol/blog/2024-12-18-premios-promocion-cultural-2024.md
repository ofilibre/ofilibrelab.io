---
title: "Premios de promoción cultural"
date: 2024-12-18
description: "La URJC fomenta la cultura abierta con concursos culturales bajo licencias libres."
type: post
categories:
  - OfiLibre
tags:
  - "OfiLibre"

bg_image: /images/logo-ofilibre.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png
---

**La URJC fomenta la cultura abierta con concursos culturales bajo licencias libres**  

Móstoles, 3 de diciembre de 2024 – La Universidad Rey Juan Carlos (URJC) celebró el acto de entrega de premios de promoción cultural, un evento que pone en valor la creatividad y el talento de su comunidad universitaria. Este año, por primera vez, todas las obras premiadas se publicarán bajo **licencias libres (Creative Commons BY-SA)**, permitiendo su difusión y reutilización abierta, en línea con los objetivos de la **Oficina de Conocimiento y Cultura Libres (OfiLibre)**.  

<div align="center">
<img src="/images/by-sa.png">
</div>
<br></br>

El acto, presidido por Mercedes del Hoyo Hurtado, Vicerrectora de Comunidad Campus, Cultura y Deporte, tuvo lugar en el Salón de Grados del Departamental I, Campus de Móstoles. En sus palabras, destacó el compromiso de la universidad con la promoción de la cultura más allá de las aulas: _“Creando y aportando fuera de las aulas \[...\] y dando valor a la capacidad creativa”_.  

Este año se sumaron dos nuevas categorías, habiendo participantes en:  

*   **Fotografía.**  
    
*   **Relatos breves**, referenciando noticias de prensa impactantes.  
    
*   **Artes plásticas**, con homenajes al año de Dalí.  
    
*   **Ensayos** sobre como afrontar la IA desde un enfoque humanista.  
    
*   **Piezas audiovisuales**, inspiradas en los relatos ganadores del año anterior.  
    

El jurado, representado por el profesor Pablo Prieto, subrayó la importancia de las bases del concurso en la cual se involucraban a toda la comunidad universitarias, desde alumnado, personal de administración y gestión, hasta profesorado._“Es la primera vez que una universidad organiza un certamen con estas características, y es un orgullo que la URJC lidere este camino hacia la cultura abierta”_, señaló. Además, desde OfiLibre recalcamos la publicación en abierto de estos proyectos, los cuales hacen uso de licencias libres tal y como establecen las bases, permitiendo que los trabajos premiados puedan compartirse abiertamente y sirvan de inspiración para futuras iniciativas. 

Desde la OfiLibre, se aplaude esta iniciativa como un paso clave hacia la sensibilización sobre la cultura libre en la universidad. Este enfoque no solo impulsa la creatividad, sino que también refleja el compromiso de la URJC por hacer el conocimiento más accesible y promover la colaboración abierta, estableciendo un precedente entre las instituciones educativas españolas.  

Con esta edición, la URJC demuestra una vez más su liderazgo en la promoción de la cultura y el conocimiento, alentando a más miembros de su comunidad universitaria a participar en las próximas ediciones y sumarse al movimiento por una cultura abierta y accesible para todos.

**Oficina de Cultura, Software y Conocimiento Libre (OfiLibre)**  
Despacho 011, planta baja edificio Rectorado. C/ Tulipán s/n, 28933 Móstoles (Madrid)  
**Correo electrónico:** [ofilibre@urjc.es](mailto:ofilibre@urjc.es)  
**Web:** [https://ofilibre.urjc.es](https://ofilibre.urjc.es)

  

[](https://ofilibre.urjc.es)

[](https://ofilibre.urjc.es/)
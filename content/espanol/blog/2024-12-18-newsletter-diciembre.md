---
title: "¡Novedades de diciembre!"
date: 2024-12-18
description: "Arrancamos el curso con emocionantes novedades en OfiLibre y os queremos contar más sobre ellas."
type: post
categories:
  - OfiLibre
tags:
  - "OfiLibre"

bg_image: /images/newsletterEmailBanner.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png
---
¡Hola, comunidad URJC! Arrancamos el curso con emocionantes novedades en OfiLibre.  

📚 <u>**Lanzamos nuestra Editorial URJC**</u>  
Explora la editorial de la URJC, un espacio para la publicación académica y el conocimiento abierto. 

👉 [Conoce más sobre la editorial haciendo aquí.](https://monografias.urjc.es/index.php/omp-urjc/index)  

📰 **<u>Plataforma de revistas URJC</u>**  
Accede a todas las revistas científicas y divulgativas de la universidad en un solo lugar.    

👉 [Consulta las revistas aquí](https://revistas.urjc.es/).  

💡 **<u>Convocatoria de asignaturas en abierto 2024-2025</u>**  
¿Eres docente? Comparte tu asignatura en abierto y fomenta el acceso libre al conocimiento.  Tienes hasta el 21 de febrero de 2025 

👉 [Consulta las bases y cómo participar](https://ofilibre.urjc.es/guias/convocatoria-asignaturas-abierto/).

🗓️ <u>**Próximas jornadas OfiLibre**</u>  
Se acercan las IV Jornadas de Cultura Libre el 26 y 27 de marzo de 2025. _**¡SAVE THE DATE!**_  
Próximamente compartiremos más detalles sobre estas actividades en nuestras redes. Pero si podemos deciros que se celebrarán unas pre-jornadas ¡No te pierdas esta oportunidad de ser parte del cambio hacia un conocimiento más libre y accesible!

👉 [Más información aquí](https://ofilibre.urjc.es/blog/jornadas-de-cultura-libre-2025/).

☕ <u>**Cafés con OfiLibre**</u>  
No te pierdas nuestros Cafés con OfiLibre, tomate una pausa de 15 minutos y aprende sobre temas clave sobre cultura libre.  

👉 [Consulta los temas e invitados aquí](https://ofilibre.urjc.es/acciones/cafes/).

¡Nos vemos en OfiLibre!    
El equipo de OfiLibre URJC.
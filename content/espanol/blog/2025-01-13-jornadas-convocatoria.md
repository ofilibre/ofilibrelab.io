---
title: "Convocatoria de Contribuciones, Jornadas de Cultura Libre 2025"
date: 2025-01-13
description: "Abrimos la convocatoria para enviar propuestas para participar en las IV Jornadas de Cultura Libre de la URJC. Este evento es una oportunidad para compartir tus ideas, conocimientos y proyectos relacionados con la cultura libre, el conocimiento libre, el software libre y la ciencia abierta."
type: post
categories:
  - OfiLibre
tags:
  - "OfiLibre"

bg_image: /images/logo-ofilibre.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png

---

Abrimos la convocatoria para enviar propuestas para participar en las IV Jornadas de Cultura Libre de la URJC ¡Si, ya vamos por las cuartas! Este evento es una oportunidad para compartir tus ideas, conocimientos y proyectos relacionados con la cultura libre, el conocimiento libre, el software libre y la ciencia abierta. Las Jornadas tendrán lugar los días 26 y 27 de marzo, en el campus de Fuenlabrada, con algunas actividades relacionadas en otros campus durante esa misma semana.

## ¿Cómo puedes participar este año?

* 🎤 Ponencias cortas: puedes dar a conocer tus trabajos en un formato breve y presencial.
* 🖼️ Posters: si prefieres una propuesta visual, los posters con una licencia libre son una buena opción para exponer el trabajo de forma creativa. Escribe la propuesta y, si es aceptada, en marzo nos mandas el material.
* ✨ Stand en la feria: si tienes un proyecto, herramienta o recurso, puedes pedir un stand para compartirlo con los asistentes.
* 🛠️ Talleres: si tienes alguna actividad práctica o dinámica que pueda interesar al público, ¡puedes organizar tu propio taller!
* 🛋️ Salas temáticas: un espacio para que todos aquellos con intereses comunes puedan reunirse, compartir ideas e interactuar sobre temas de interés.
* 🎨 Otras formas de participar: si se te ocurre alguna otra manera de aportar a las jornadas, estamos abiertos a nuevas propuestas. ¡Queremos hacer de este evento algo único y diverso!

## Temas:

Las formas de participación podrán ser sobre cualquiera de los siguientes temas:

* Experiencias relacionadas con ciencia abierta y laboratorios ciudadanos
* Experiencias relacionadas con la publicación en abierto de materiales docentes
* Experiencias de uso de materiales en abierto realizados por terceros.
* Experiencias relacionadas con la publicación en abierto de resultados de investigación.
* Experiencias relacionadas con la publicación o uso de datos abiertos en actividades docentes, de investigación, o de gestión.

## Detalles importantes

* ⏰ La fecha límite para presentar propuestas es el Viernes 14 de febrero ❤️​ de 2025.
* 📥 Puedes enviar tu propuesta a través del siguiente [formulario](https://forms.office.com/e/8izm4HGNsf)
* 📧 Si tienes cualquier consulta, no dudes en escribirnos a ofilibre@urjc.es, indicando como asunto "Jornadas de Cultura Libre".

Si lo que te interesa es participar como asistente, ¡no te preocupes! Las inscripciones para asistir abrirán más adelante, a finales de febrero de 2025. 

¡Esperamos ver tu propuesta y compartir este evento tan importante con toda la comunidad universitaria!


**¡Infórmate y participa!** Mantente al tanto en nuestras redes sociales. Toda la información actualizada y las fechas clave las encontrarás en:  

*   **Web:** [https://ofilibre.urjc.es/](https://ofilibre.urjc.es/)
*   **Mastodon:** [@OfiLibreURJC@floss.social](https://floss.social/@ofilibreurjc)
*   **Bluesky:** [@ofilibre.bsky.social](https://bsky.app/profile/ofilibre.bsky.social)
*   **Twitter/X:** [@OfiLibreURJC](https://x.com/OfiLibreURJC)  
c)
*   **Telegram:** [https://t.me/ofilibreurjc](https://t.me/ofilibreurjc)
*   **Instagram:** [@ofilibreurjc](https://www.instagram.com/ofilibreurjc/)

---
title: "¿Dónde está la licencia?"
date: 2024-09-18
description: "¿Cómo sé qué licencia tiene el artículo que publiqué en tal o cual revista? ¿Cómo puedo saber bajo qué condiciones se ha publicado un material? Encuentra la respuesta a estas y otras preguntas en el vídeo ¿Dónde está la licencia?"
type: post
categories:
  - OfiLibre
  - "Cultura libre"
tags:
  - "Licencias"
  - "Vídeos"
  - "Formación"
  - "Cultura libre"
  - "Acceso abierto"

bg_image: /images/openaccess-square.png
image: /images/openaccess-square.png
thumb: /images/openaccess-square.png

---

<iframe src='https://tv.urjc.es/iframe/66ab576943c84969a63ad8b6' id='pumukitiframe' frameborder='0' border='0' width='100%' height='500px' allowfullscreen></iframe>

Te ofrecemos el vídeo [¿Dónde está la licencia?](https://tv.urjc.es/video/66ab576943c84969a63ad8b6), para ayudar a identificar licencias de acceso abierto en revistas de investigación.

En la OfiLibre, nos encontramos a menudo con la siguiente pregunta: "¿Cómo sé qué licencia tiene el artículo que publiqué en tal o cual revista?" o "¿Cómo puedo saber bajo qué condiciones se ha publicado un material?". Conocer la licencia de uso de una revista o artículo es fundamental, pero no siempre recordamos lo que firmamos al publicar un artículo. A menudo, en nuestro afán por publicar, simplemente hacemos clic en "aceptar" sin tener claro qué implicaciones tiene ni qué condiciones de uso se aplicarán a nuestro material una vez que forme parte de la revista.

Al publicar un artículo, un libro, o incluso un texto en un blog, probablemente estamos cediendo ciertos derechos, mientras que nos reservamos otros. Si la revista o institución con la que tratamos utiliza licencias Creative Commons, todo se simplifica, ya que son estándares bien conocidos y fáciles de entender. Sin embargo, esto no siempre es evidente, y a menudo necesitamos investigar las condiciones de uso.

Para facilitar esta tarea, desde la OfiLibre y con la colaboración de Oscar Cabrera, que ha trabajado con nosotros con una beca de formación, hemos creado este vídeo inspirado en el clásico "Dónde está Wally" de Martin Handford. Esperamos que sea de ayuda para identificar qué se puede o no se puede hacer con un artículo ya publicado.




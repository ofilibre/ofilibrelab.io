---
title: "Canales de comunicación de OfiLibre"
date: 2024-11-13
description: "La OfiLibre tiene varios canales que puedes consultar (o suscribirte a ellos) si quieres seguir la información que proporcionamos."
type: post
categories:
  - OfiLibre
tags:
  - "Redes sociales"

bg_image: /images/logo-ofilibre.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png

---

Si quieres seguir la información que difundimos desde la OfiLibre, estos son los canales que usamos:

* Sitio web: https://ofilibre.urjc.es
* Dirección de correo: ofilibre@urjc.es 
* Redes sociales:

  * X (Twitter): [@OfilibreURJC](https://x.com/OfiLibreURJC) 
  * Instagram: [@OfilibreURJC](https://www.instagram.com/OfiLibreURJC) 
  * Mastodon: [@ofilibreurjc@floss.social](https://floss.social/@OfiLibreURJC) 
  * Telegram: [Canal ofilibreurjc](https://t.me/ofilibreurjc)
  
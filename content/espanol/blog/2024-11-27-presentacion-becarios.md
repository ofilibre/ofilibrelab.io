---
title: "¡Comenzamos el curso con becarios!"
date: 2024-11-27
description: "Durante este curso contamos con un nuevo y excepcionalmente amplio grupo de Becarios"
type: post
categories:
  - OfiLibre
tags:
  - "OfiLibre"

bg_image: /images/logo-ofilibre.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png

---

Desde sus orígenes, la Oficina de Conocimiento y Cultura Libres (OfiLibre) ha apostado por desempeñar una labor formativa significativa, ofreciendo Becas de Formación para abrir sus puertas al estudiantado interesado en la cultura libre. Este programa no solo refuerza nuestro compromiso con el acceso universal al conocimiento, sino que también reconoce el papel clave de las nuevas generaciones en la transformación del panorama académico y social.  

Durante este curso, contamos con una nueva y excepcionalmente amplia remesa de Becarios, quienes, en los próximos meses, aportarán su granito de arena y sus nuevas perspectivas para hacer que la cultura libre llegue a cada rincón de la Universidad Rey Juan Carlos. Este año, se sumarán al equipo:  

*   Helena Rodríguez Castellanos y Mariam Ben-Taieb Ibn-Taieb en Comunicación.
*   Pablo Rubio Ramos, beneficiario de la Beca de Formación Editorial.  
*   David Huertas Salas y José Jesús Durán Quintana en Apoyo Informático.      
*   Neiser Aguayo Linares como Becario de Diseño y Creación Audiovisual.  

Tenemos la seguridad de que su dedicación y creatividad nos permitirán seguir avanzando con energía renovada, consolidando la misión de OfiLibre como un espacio de formación y crecimiento en el ámbito de la cultura libre.

<img src="/images/blog/becarios-2024.jpg" alt="Becarios del curso 2024-2025" width="600">

¡Os damos la bienvenida al equipo! Estamos deseando ver todas las ideas y proyectos que desarrollaremos en conjunto en los próximos meses.  


### Pero… ¿y dónde está la OfiLibre?**  

Si quieres saber más sobre nuestro trabajo o visitarnos:  

*   **Oficina:** Despacho 011, Edificio de Rectorado, Campus de Móstoles.  
    
*   **Web:** [https://ofilibre.urjc.es/](https://ofilibre.urjc.es/)  
    
*   **Twitter:** [@OfiLibreURJC](https://x.com/OfiLibreURJC)  
    
*   **Mastodon:** [@OfiLibreURJC@floss.social](https://floss.social/@ofilibreurjc)
    
*   **Bluesky:** [@ofilibre.bsky.social](https://bsky.app/profile/ofilibre.bsky.social)  
    
*   **Canal de Telegram:** [https://t.me/ofilibreurjc](https://t.me/ofilibreurjc)  
    
*   **Instagram:** [@ofilibreurjc](https://www.instagram.com/ofilibreurjc/)  
    

¡Te invitamos a seguirnos en nuestras redes y a unirte a la conversación sobre cultura y conocimiento libres!
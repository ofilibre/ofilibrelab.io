---
title: Presentación de convocatoria de asignaturas en abierto
date: 2024-09-02
description:  La Universidad Rey Juan Carlos ha vuelto a publicar su convocatoria para el reconocimiento del esfuerzo en publicar materiales docentes en abierto. Esta presentación explicará las principales características de la convocatoria.
type: post
categories:
  - OfiLibre
tags:
  - "URJC"
  - "Asignaturas en abierto"
  - "Acceso abierto"
  - "Convocatorias"

bg_image: /images/logo-urjc-square.png
image: /images/transpas/convocatoria-asignaturas-abierto/Convocatoria_Asignaturas_Abierto.png
thumb: /images/logo-urjc-square.png

---


El próximo miércoles 4 de septiembre tendremos una videoconferencia de presentación de la convocatoria 2024-2025 de reconocimiento a la publicación de asignaturas en acceso abierto. Como en cursos anteriores, esta convocatoria pretende fomentar la publicación de materiales docentes en acceso abierto, mediante ciertos incentivos y reconocimientos al personal docente que los ha producido y los utiliza en sus clases.

* Videoconferencia de presentación: miércoles 4 de septiembre, 11:00, [enlace Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZjYxYjliNDUtZTZhNS00YmIyLWI5NTgtNmRhZDdlOWQ5ZTE0%40thread.v2/0?context=%7b%22Tid%22%3a%225f84c4ea-370d-4b9e-830c-756f8bf1b51f%22%2c%22Oid%22%3a%22f39a6111-b3eb-43a6-98c0-a4d0f78c6742%22%7d)

* [Detalles de la convocatoria](https://ofilibre.urjc.es/guias/convocatoria-asignaturas-abierto/), incluyendo enlace al texto oficial, e información sobre la videoconferencia de presentación.

* Ejemplos de asignaturas en acceso abierto reconocidas en convocatorias anteriores: [zona de asignaturas en acceso abierto del aula virtual de la Universidad](https://online.urjc.es/es/para-futuros-estudiantes/asignaturas-en-abierto).
 

---
title: "Pósters de las III Jornadas de Cultura Libre"
date: 2025-01-30
description: "Conjunto de pósters relacionados con la cultura libre utilizados en las III Jornadas."
type: post
categories:
  - OfiLibre
tags:
  - "OfiLibre"
bg_image: /images/newsletterEmailBanner.png
thumb: /images/posters-iii-jornadas.png
---
Aquí se encuentran todos los pósters que se presentaron en las III Jornadas de Cultura Libre. Recordad que también tenemos entradas en el blog dedicadas a los [contenidos](https://ofilibre.urjc.es/blog/programa-iii-jornadas/), así como un [resumen](https://ofilibre.urjc.es/blog/resumen-iii-jornadas/) de estas Jornadas. Esperamos que os gusten y, si los queréis usar, ¡recordad citar autoría!

* **Climate Warriors**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/01_Carbonell_Alcocer_ClimateWarriors_A1_page-0001.jpg" alt="Climate Warriors">

* **Proyecto de investigación +UniversiDATA**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/02_Gálvez_De-la-Cuesta_+Universidata_A1_page-0001.jpg" alt="Proyecto de investigación +UniversiDATA">

* **Programa de Intervención Educativa para la Promoción de Conductas Prosociales Online en la Adolescencia**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/03_Pérez_Torres_Be-Prosocial_A1_page-0001.jpg" alt="Conductas Prosociales Online en la Adolescencia"> 

* **Wikipedia y fuentes documentales**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/04_Martínez_Valerio_Wikipedia-y-fuentes-documentales_A2_compressed_page-0001.jpg" alt="Wikipedia y fuentes documentales"> 

* **Publicación en abierto de técnicas de _Visual Thinking_ realizadas por estudiantes de Ciencias de la Salud**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/05_González_Poster_Visual-Thinking__A1_page-0001.jpg" alt="Publicación en abierto de técnicas de Visual Thinking"> 

* **Díptico de educación - Wikimedia España**

<img class="imagen-centrada mt-20" src="/images/blog/posters-jornadas-2024/06_Wikimedia_Díptico-Educación_Doble-Cara_A4_page-0001.jpg" alt="Diptico de educación (I)">

<img class="imagen-centrada mb-20" src="/images/blog/posters-jornadas-2024/06_Wikimedia_Díptico-Educación_Doble-Cara_A4_page-0002.jpg" alt="Diptico de educación (II)"> 

* **Jardines Digitales - nodo común**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/07_NODO-COMUN_Jardines-digitales_A2_page-0001.jpg" alt="Jardines digitales">

* **Open Source Robotics**

<img class="imagen-centrada mbt-20" src="/images/blog/posters-jornadas-2024/08_GARCÍA_GÓMEZ_OPEN-SOURCE_A2_page-0001.jpg" alt="Open Source Robotics">

* **VIRION (Virtual Reality applied to school education)**

<img class="imagen-centrada mt-20" src="/images/blog/posters-jornadas-2024/09_VIRION_Folleto_Doble-Cara_A5_page-0001.jpg" alt="Virtual Reality applied to school education (I)">

<img class="imagen-centrada mb-20" src="/images/blog/posters-jornadas-2024/09_VIRION_Folleto_Doble-Cara_A5_page-0002.jpg" alt="Virtual Reality applied to school education (II)"> 
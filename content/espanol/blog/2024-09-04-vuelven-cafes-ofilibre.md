---
title: ¡Vuelven los cafés de la OfiLibre!
date: 2024-09-04
description:  Este nuevo curso volvemos a tener cafés los miércoles a las 10:00, en directo a través de TV-URJC.
type: post
categories:
  - OfiLibre
tags:
  - "URJC"
  - "Café con OfiLibre"

bg_image: /images/logo-ofilibre.png
image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png

---


El próximo miércoles 11 de septiembre... ¡vuelven los cafés de la OfiLibre! Comenzaremos el curso hablando de los planes que tenemos, de cómo puedes estar al tanto de lo que hacemos, y de cuáles son las acciones más relevantes que está preparando la Universidad en lo relacionado con el acceso abierto, la cultura libre, y todos estos temas que nos interesan en la OfiLibre.

* [Cafés de la OfiLibre (en directo vía TV URJC](https://tv.urjc.es/live/event/661cfe7b43c849c8e7614ed9), los miércoles a las 10:00.

* [Información sobre los cafés de la OfiLibre](/acciones/cafes/), incluyendo el plan de temas para los próximos cafés, y enlaces a las grabaciones de los que ya tuvieron lugar.

* [Grabaciones de cafés pasados en TV URJC](https://tv.urjc.es/series/655f2053f8ceb778a509d85f).
---
title: Resumen de las III Jornadas de Cultura Libre 2024
date: 2024-04-16
description:  Resumen de las III Jornadas de Cultura Libre 2024
slug: resumen-III-jornadas
type: post
categories:
  - OfiLibre
tags:
  - "Jornadas"
  - "URJC"
  - "Cultura libre"

bg_image: /images/CONTENIDO_III_JORNADAS_bg.png
thumb: /images/RESUMEN-III-Jornadas-thumb.jpg
---
Nuestras III [*Jornadas de Cultura Libre*](/blog/programa-iii-jornadas/) han sido todo un éxito. 

Este evento, que cada vez tiene más seguidores, tuvo lugar los días 20 y 21 de marzo de 2024 en el edificio de Gestión de Fuenlabrada de la URJC.

Esta nueva convocatoria citó a alumnos, profesores y profesionales del mundo de la investigación con un interés común: incentivar el uso de la CULTURA LIBRE. 

El acto de apertura de estas jornadas estuvo a cargo de nuestra Vicerrectora Mercedes del Hoyo Hurtado junto a Jesus González Barahona que realizaron uno de nuestros "Cafés con la OfiLibre". 

Contamos con la participación de varios ponentes invitados: 
- Remedios Pérez García *"Del Acceso Abierto a la Ciencia Abierta: Retos de la Edición Técnica"*
- Sonia Castro García-Muñoz *"Datos en Abierto: Movimiento de Expansión"*.

 Además, como novedad, hemos tenido una tarde dedicada a la Inteligencia Artificial Abierta que incluyó la presencia de: 
 - Jose Antonio Gelado *"Podcast Libre. Podcast e Inteligencia Artificial"*
 - Pablo Aragón *"Wikimedia Research en la Era de la Inteligencia Artificial"*
 - Pedro Cuenca *"Inteligencia Artificial Generativa Abierta"*.

<br/>

Asimismo, No podemos olvidar la Mesa Redonda (Logros y retos de las revistas URJC de acceso abierto: Un diálogo compartido con la comunidad científica) moderada por Tomás Zarza quien también tuvo un pequeño momento donde se le presentó como nuevo Coordinador de Publicaciones en Abierto y nuevo integrante de la OfiLibre. 

Pasear por la feria, participar en uno de los talleres, asistir a una representacion teatral y tomar un café entre personas con la misma inquietud por la cultura libre han convertido estas jornadas en un evento único de nuestra universidad.

Los pósters utilizados fueron recopilados en [esta entrada del blog.](../pósters-de-las-iii-jornadas-de-cultura-libre)

Gracias a todos los participantes y asistences. Esperamos veros el año que viene. 

[**Enlace a TV URJC**](https://tv.urjc.es/series/65f1c4c5a848b9cfa425be43) donde podreis ver todas las jornadas en diferido. 

### Números de las III Jornadas

- Asistentes a las jornadas: 206
- Asistentes a talleres: 62
- Número de talleres: 4
- Presentacion de revistas: 7
- Stands de feria: 8
- Ponencias cortas: 21
- Ponencias invitadas: 2
- Charlas: 7
- Posters: 12
- Lectura dramatizada: 1 (Grupo de teatro de la URJC: Arrojo escénico)
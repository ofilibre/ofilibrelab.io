---
title: "Normativa relacionada con OfiLibre"
date: 2025-01-07
slug: normativa
draft: false
author: "OfiLibre"
type: "post"
tags: ["OfiLibre", "cultura libre", "normativa"]
categories: ["acciones"]

description: "Normativa (reglamentos, desarrollos de reglamentos, etc.) relacionados con OfiLibre."

# post images
bg_image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png
---

Algunas de las principales normativas de la URJC relacionadas con las competencias de la OfiLibre:

* [Reglamento de Publicación en Abierto y Cultura Libre de la Universidad Rey Juan Carlos](/documentos/normativa/Reglamento_Publicacion_Abierta-2024-11-08.pdf), aprobado por acuerdo del Consejo de Gobierno en sesión del 3 de octubre de 2024. Publicado en el [Boletín de la URJC de 8 de noviembre de 2024](/documentos/normativa/BOURJC-2024-11-08.pdf).


* [Reglamento de la Editorial Academia Abierta de la Universidad Rey Juan Carlos](/documentos/normativa/Reglamento_Editorial-2024-11-08.pdf), aprobado por acuerdo del Consejo de Gobierno, en sesión del 3 de octubre de 2024. Publicado en el [Boletín de la URJC de 8 de noviembre de 2024](/documentos/normativa/BOURJC-2024-11-08.pdf).

Nota: Pueden consultarse los [ejemplares del Boletín de la URJC en la Sede Electrónica de la Universidad](https://sede.urjc.es/boletines-publicados)


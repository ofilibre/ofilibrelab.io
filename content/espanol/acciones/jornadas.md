---
title: "Jornadas de Cultura Libre"
date: 2024-03-18
slug: jornadas
draft: false
author: "OfiLibre"
type: "post"
tags: ["OfiLibre", "cultura libre", "Jornadas de Cultura Libre", "jornadas"]
categories: ["acciones"]

description: "Jornadas de Cultura Libre: unos días para celebrar la cultura libre, la publicación en abierto, los datos abiertos, el software libre..."

# post images
bg_image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png
---

Desde 2022 se celebran, anualmente, las Jornadas de Cultura Libre de la URJC:

* IV Jornadas de Cultura Libre (26 y 27 de marzo de 2025): [primer anuncio](/blog/2024-11-27-jornadas), [convocatoria de contribuciones](/blog/2025-01-13-jornadas-convocatoria)

* III Jornadas de Cultura Libre (20 y 21 de marzo de 2024): [programa final, incluyendo materiales](/blog/programa-iii-jornadas/), [posters](/blog/pósters-de-las-iii-jornadas-de-cultura-libre/), [anuncio](/blog/2024-01-10-jornadas/)

* II Jornadas de Cultura Libre (29 y 30 de marzo de 2023): [programa final, incluyendo materiales y vídeos](https://ofilibre.urjc.es/blog/resumen-segundas-jornadas-2023/), [anuncio](/blog/jornadas-cultura-libre-2/)

* I Jornadas de Cultura Libre (30 de marzo de 2022): [programa final, incluyendo materiales y vídeos](/blog/2022-05-19-jornadas-resultado/), [anuncio](/blog/jornadas-cultura-libre/)

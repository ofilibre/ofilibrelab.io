---
title: "Café con la OfiLibre"
date: 2024-11-02
slug: cafes
draft: false
author: "OfiLibre"
type: "post"
tags: ["OfiLibre", "cultura libre"]
categories: ["acciones"]

description: "Café con la OfiLibre. Unos minutos cada semana con la cultura libre, al publicación en acceso abierto, el software libre, los datos abiertos, y todas estas cosas que te interesan"

# post images
bg_image: /images/logo-ofilibre.png
thumb: /images/logo-ofilibre.png
---

Todos los miércoles lectivos, a las 10:00, nos vemos durante 15 minutos en el Café con la OfiLibre, por videoconferencia. Un rato para charlar de forma relajada sobre los temas que nos preocupan. Cada día tratamos un tema: sólo tienes que conectarte y escuchar. Y participar, si te apetece. Al fin y al cabo, una forma de mantener contacto entre los que estamos interesados en la cultura libre, la publicación en acceso abierto, el software libre, los datos abiertos, y todos estos jaleos. Eso sí, el café (o el té, o el vaso de agua, o lo que te apetezca) lo tienes que traer tú.

> [Café con la OfiLibre en directo](https://tv.urjc.es/live/event/661cfe7b43c849c8e7614ed9)

Programa (tentativo) para los próximos cafés:

* 5 de marzo: Ley de la Ciencia, propiedad intelectual y fomento de la reversión a la comunidad, con José Antonio Castillo Parrilla (Investigador Ramón y Cajal en el Departamento de Derecho Civil de la Universidad de Granada)

* 12 de marzo: Con Manuel Palomo de la oficina de Software libre de la Universidad de Cádiz

Cafés ya disfrutados ([videos de todos estos cafés](https://tv.urjc.es/series/655f2053f8ceb778a509d85f)):

* 26 de febrero: Accesibilidad en el mundo de las publicaciones académicas, con Marcos Ferreira Sanmamed (Glaux, empresa de apoyo a la editorial Academia Abierta de la URJC)

* 19 de febrero: Inteligencia Artificial desde la cultura libre

* 12 de febrero: [Asignaturas en abierto (segunda parte)](https://tv.urjc.es/video/67aeeff79978f3b1831bf371)

* 5 de febrero: [Asignaturas en abierto (primera parte)](https://tv.urjc.es/video/67aef2299978f3b18f10953b)

* 29 de enero: Revista de Innovación Docente en el Aula Universitaria

* 22 de enero: [Presentación del proyecto ALMASI (mutualización de servicios de ciencia abierta)](https://tv.urjc.es/video/67a330f29978f37ad90c5dd1)

* 15 de enero: [Jornadas de Cultura Libre y Aniversario de Wikipedia](https://tv.urjc.es/video/6799fd7f9978f36a07309692).

* 18 de diciembre: [Repaso del año 2024](https://tv.urjc.es/video/677b8eb49978f3350b5a3041).

* 11 de diciembre: [Publicar en Abierto](https://tv.urjc.es/video/677b91f19978f335181972cc), con Laura de la Cruz (Jefa de Servicio de Publicaciones). Revistas en abierto, estándares de publicaciones académicas...

* 4 de diciembre: [El proyecto Creative Commons (segunda parte)](https://tv.urjc.es/video/675fefcd9978f37061497533).

* 27 de noviembre: [El proyecto Creative Commons (primera parte)](https://tv.urjc.es/video/67596cdf9978f3016e1195a2).

* 20 de noviembre: [Visita al Congreso de la Unión de Editoriales Universitarias Españolas](https://tv.urjc.es/video/6747025b9978f310f17833dc).

* 13 de noviembre: [Redes federadas](https://tv.urjc.es/video/674477d59978f351f44de4ad). ¿Qué es el Fediverso, Mastodon, y todo eso? ¿Qué caracteriza las redes sociales federadas, frente a las centralizadas, como Facebook, X, Instagram o Tiktok?

* 6 de noviembre: [Arqueología y recursos educativos abiertos](https://tv.urjc.es/video/674702449978f310f06bcc42), con Alberto Polo (URJC).

* 30 de octubre: [Tipos de letra libres](https://tv.urjc.es/video/672327b843c8491bb40f216b), con José Vélez Serrano (URJC)

* 23 de octubre: [¿Niveles de libertad?](https://tv.urjc.es/video/6723247043c8491af3428d5d) Cómo el software te quita o te da libertad.

* 16 de octubre: [Biblioteca y Semana del acceso abierto](https://tv.urjc.es/video/670f90eb43c849495d329432), con Fernando Silva (Biblioteca URJC).

* 9 de octubre: [Revista ICONO 14](https://tv.urjc.es/video/670b7a4143c84956df212678) (una de las [revistas de la URJC](https://revistas.urjc.es/), con Manuel Gertrudix (editor de ICONO 14). 

* 2 de octubre: [Jornadas de Innovación docente](https://tv.urjc.es/video/66fec7d943c8499bd864d552), con Irene Ros (Coordinadora Académica del Programa Innovación Docente, CIED).

* 25 de septiembre: [Declaración de Barcelona sobre la información abierta de investigación](https://tv.urjc.es/video/66f70cba43c849b6f33fc5e2).

* 18 de septiembre: [Convocatoria de asignaturas en acceso abierto](https://tv.urjc.es/video/66ec871d43c8499db5643342)

* 11 de septiembre: [Comienzo de curso: qué haremos en 2024-2025](https://tv.urjc.es/video/66e87b8043c84960e7766cf4)

* 26 de junio: [El curso 2023-2024 visto desde la OfiLibre](https://tv.urjc.es/video/667d7f7a43c849ef446ed6f6).

* 19 de junio: [Software libre en la URJC](https://tv.urjc.es/video/667bf0ba43c849b5a17c2980), con Micael Gallego (Director Académico del Programa para la Transformación Digital, URJC).

* 12 de junio: [Recursos educativos en abierto](https://tv.urjc.es/video/667c296d43c849c11d0b89d7), con Oriol Borrás (Coordinador Académico del Programa de Tecnologías Educativas del CIED, URJC).

* 5 de junio: Ciclo de revistas en abierto de la URJC: [Guerra Colonial](https://tv.urjc.es/video/667bf40443c849b6db623444), con Miguel Madueño (Director de la revista Guerra Colonial).

* 29 de mayo: [El congreso esLibre](https://tv.urjc.es/video/66572cdd43c84987b8317a72)

* 22 de mayo: [Revistas en abierto de la URJC](https://tv.urjc.es/video/6656f68743c84972826ab6ec), con Laura de la Cruz (responsable de Publicaciones, URJC). ¿Cómo es el programa de publicación de revistas en acceso abierto de la URJC? ¿Cómo se puede participar en él? ¿Qué relación tiene con la ciencia abierta?

* 8 de mayo: [Archivos públicos de datos abiertos](https://tv.urjc.es/video/664df0a843c84998e868d785), con Marta Ortiz de Urbina (URJC), Carmen de Pablos (URJC) y Alberto Abella (FIWARE). El mundo de los datos abiertos, la importancia de que las administraciones públicas y otras instituciones los produzcan, y lo importante que es que se ofrezcan con la calidad adecuada.

* 24 de abril: [Modelos generativos de IA abiertos](https://tv.urjc.es/video/6630bca143c849ecde7bb7e2?track_id=6630be9d43c849ed8d574cc2). Una (muy rápida y superficial) visita al mundo de los modelos de IA generativos que puedes instalar en tu ordenador.

* 17 de abril: [Trabajos fin de grado en abierto](https://tv.urjc.es/video/6626935443c849b9ae5cdf20). ¿Cómo se publica un trabajo fin de grado en abierto? ¿Por qué le puede interesar al alumno? ¿Qué hay que hacer con la memoria del TFG para poder publicarla en abierto? ¿Qué consecuencias tiene esa publicación?

* 10 de abril: [Cómo funciona Wikipedia](https://tv.urjc.es/video/661f7dde43c849604343ebc3). Los procesos de edición, la calidad de los artículos, la sostenibilidad del proyecto, la vida de los editores...

* 3 de abril: Revistas en acceso abierto

* 20 de marzo: [Apertura de las III Jornadas de Cultura Libre](https://tv.urjc.es/video/6630bf7943c849ee787c2085), con Mercedes del Hoyo (Vicerrectora de Comunidad Campus, Cultura y Deporte, URJC). Cómo vemos la cultura libre en la URJC.

* 13 de marzo: [Programa de las III Jornadas de Cultura Libre](https://tv.urjc.es/video/65f835a0a848b9f23a3206e2). Presentamos y comentamos el progrmaa de las Jornadas de Cultura Libre de este año.

* 21 de febrero: [Revistas de acceso abierto de la URJC](https://tv.urjc.es/video/65d8471ea848b9c51921ad63), con Laura de la Cruz, Servicio de Publicaciones de la URJC. ¿Qué es el servicio de publicaciones de la URJC? ¿Cómo funciona la convocatoria de revistas en abierto? ¿Cúales son los pasos a seguir para publicar una revista en abierto? ¿Qué es el proyecto de monografías?

* 14 de febrero: [Datos abiertos](https://tv.urjc.es/video/65d4b570a848b90ab40ad74c), con Adrián Escuedero Alcántara, catedráticon en la ESCET. ¿Qué quiere decir datos abiertos? ¿Cómo trabaja el grupo de investigación de alto rendimiento en Ecología de comunidades de la URJC con los datos? ¿Cómo obtiene el grupo los datos de investigación? ¿Qué diferencias hay a la hora de publicar los datos? ¿Qué es realmente "publicar datos abiertos"?

* 7 de febrero: [Licencias en software libre](https://tv.urjc.es/video/65d4b7daa848b90bb448ff75). Cómo funcionan las licencias en software libre, quién puede crear una licencia de software libre, qué es realmente el software libre, y mucho más.

* 31 de enero: [Monografías en abierto](https://tv.urjc.es/video/65c9b5cda848b9e858770cf2). Explicamos la convocatoria de publicación de Monografías en abierto de la URJC.

* 24 de enero: [Jornadas de Cultura Libre](https://tv.urjc.es/video/65c9b914a848b9e86870130c). Qué son las Jornadas de Cultura Libre, quiénes pueden asistir, cómo participar en ellas...

* 17 de enero: [Sexenios con publicación en abierto](https://tv.urjc.es/video/65ae0ed1e8dbc0a31461c4cf). Algunos comentarios sobre los aspectos relacionados con la publicación en abierdo de la convocatoria de sexenios de investigación.

* 10 de enero: [Cómo se plantea el 2024](https://tv.urjc.es/video/65a256c8e8dbc047d736a4ac). Comentarios sobre los plantes que tenemos en la OfiLibre para este año que empieza.

* 20 de diciembre: [Sexenios y Repositorio Abierto](https://tv.urjc.es/video/65859b2bf8ceb7499c24511c). Publicación en abierto de aportaciones para la Convocatoria de Sexenios de Investigación

* 13 de diciembre: [Software libre en la Universidad](https://tv.urjc.es/video/6582e0b3f8ceb77fd56001ed). Repaso a algunos programas libres que usamos en la Universidad, algunos de ellos fundamentales para nuestro trabajo diario.

* 29 de noviembre: [Trabajos de Fin de Grado en abierto](https://tv.urjc.es/video/65816fa1f8ceb7097e39045d). Cómo publicar la memoria del TFG en abierto, qué supone hacerlo, cómo hay que prepararlo...

* 22 de noviembre: Asignaturas en acceso abierto, incluyuendo el anuncio de la apertura del formulario para las asignaturas del segundo cuatrimestre del curso 2023-2024.

* 15 de noviembre: [Publicación en acceso abierto y software libre](https://tv.urjc.es/video/655f23abf8ceb77a9924a4d2). Contaremos con la colaboración de José Luis Aznarte Mellado, Director de la División de Evaluación de Enseñanzas e Instituciones de ANECA y profesor en la UNED.

* 8 de noviembre: [Revistas en acceso abierto](https://tv.urjc.es/video/65814c34f8ceb7f95772f4a2). Resumen sobre el Programa de Apoyo a la Publicación de Revistas Académicas en Acceso Abierto del Vicerrectorado de Comunidad Campus, Cultura y Deporte

* 25 de octubre: [Presentación de Café con la OfiLibre](https://tv.urjc.es/video/655f5eb4f8ceb7984c018f81). 20 años de la [Declaración de Berlín sobre Acceso Abierto](https://openaccess.mpg.de/Berlin-Declaration).



---
title: Reconocimiento de publicación de asignaturas en abierto 2024-2025
date: 2024-07-16
logo: logo-urjc-square.png
type: guias
---


**Solicitudes:**

Convocatorias en la [sede virtual de la Universidad, sección de convocatorias](https://sede.urjc.es/convocatorias).

* Asignaturas primer cuatrimestre: Hasta el 15 de noviembre de 2024 para asignaturas del primer cuatrimestre (pero solo se garantiza la tramitación de documentos subidos a BURJC Digital hasta el 13 de noviembre, incluido).

* Asignaturas segundo cuatrimestre: Hasta el 21 de febrero de 2025 para asignaturas del segundo cuatrimestre  (pero solo se garantiza la tramitación de documentos subidos a BURJC Digital hasta el 19 de febrero, incluido).


**Material auxiliar:**

* [Infografía-resumen de la convocatoria](/documentos/convocatoria-asignaturas-abierto-2024-2025/Mapa_Conceptual.pdf)
* [Guía-resumen para rellenar el formulario de solicitud](/documentos/convocatoria-asignaturas-abierto-2024-2025/Guia.pdf)
* Ejemplos de asignaturas en acceso abierto reconocidas en convocatorias anteriores: [zona de asignaturas en acceso abierto del aula virtual de la Universidad](https://online.urjc.es/es/para-futuros-estudiantes/asignaturas-en-abierto).
* El miércoles 4 de septiembre se ofreció una sesión telemática pública sobre la convocatoria:
  * [transparencias utilizadas durante la presentación](/transpas/convocatoria-asignaturas-abierto/Convocatoria_Asignaturas_Abierto.pdf)
  * [grabación en vídeo de la sesión](https://tv.urjc.es/video/66e7fa5b43c849203b1e4b85)
  
**Características principales:**

Esta tercera convocatoria se realiza en la Universidad Rey Juan Carlos para promover entre los docentes la creación de recursos educativos abiertos de calidad, y fomentar su uso en las asignaturas que dichos docentes imparten. La convocatoria es iniciativa de los Vicerrectorados de Transformación Digital e Innovación Docente, Comunidad Campus, Cultura y Deporte, y Ordenación Académica y Formación del Profesorado.

Los detalles de la convocatoria pueden consultarse en el documento [Convocatoria para el reconocimiento de publicación de asignaturas en acceso abierto 2024-2025](/documentos/convocatoria-asignaturas-abierto-2024-2025.pdf). A continuación se resumen y se explican algunos de sus aspectos más importantes. En caso de diferencia entre este resumen y la convocatoria, tendrá validez la convocatoria, que es el oficialmente publicado por la Universidad.


### Objeto

Esta convocatoria pretende promover el trabajo del personal docente de la URJC para que publiquen sus materiales en asignaturas en acceso abierto, evaluándose dicho trabajo y asignando, en su caso, un incentivo económico y otros beneficios y efectos, por los que se reconoce el esfuerzo realizado en la elaboración de materiales publicados durante el curso 2024-25.

### Participación

La participación en esta convocatoria se hace proponiendo asignaturas del [aula virtual de la URJC](https://aulavirtual.urjc.es), que tendrán que ser de docencia oficial en un grado o máster universitario de la URJC durante el curso 2024-2025. Los materiales docentes de esas asignaturas se habrán publicado previamente en abierto.

### Publicación de materiales en acceso abierto

Los materiales que se sometan a evaluación deben tener en [lugar visible la licencia bajo la que se publican](/blog/publicar-abierto/), que tendrá que ser una de las licencias de publicación en acceso abierto aprobadas por el Consejo de Publicación Abierta de la URJC, con el consentimiento de todos sus autores (licencias [Atribución](https://creativecommons.org/licenses/by/4.0/deed.es) o [Atribución-CompartirIgual](https://creativecommons.org/licenses/by-sa/4.0/deed.es) de Creative Commons).

Los materiales en formato bibliográfico (guías, apuntes, colecciones de problemas y ejercicios, colecciones de exámenes, presentaciones, etc.) deben publicarse en el repositorio abierto institucional, [BURJC Digital](https://burjcdigital.urjc.es). Los vídeos y audios deben subirse a [TV URJC](https://tv.urjc.es/).

Puede consultarse el documento [Cómo publicar materiales docentes en abierto](/guias/materiales-docentes-abierto/) para entender el proceso completo de publicación en abierto de materiales docentes. En el caso específico de podcast y vídeos, debe consultarse también el [Procedimiento de publicación de materiales en abierto en TV URJC](https://urjc.atlassian.net/wiki/spaces/BDCP/pages/9899928/Procedimiento+de+subida+de+v+deos+Convocatoria+para+el+reconocimiento+de+publicaci+n+de+asignaturas+en+acceso+abierto).

### Presentación de solicitudes

A la convocatoria se podrán presentar asignaturas individuales, entendiendo como tales asignaturas que se presentan como tales en el aula virtual. En el caso de grupos de asignaturas que compartan los mismos materiales (por ejemplo, distintos grupos de la misma asignatura, o asignaturas similares en distintos grados) se presentarán como una única asignatura agrupada.

Para formalizar la solicitud para una asignatura o grupo de asignaturas, una vez sus materiales se hayan publicado en abierto, la realizará el responsable de grupo de actas de una de las asignaturas agrupadas (según figure en el Plan de Ordenación Docente) rellenando los datos solicitados en el procedimiento de solicitud para esta convocatoria que se encuentra en la sede electrónica de la Universidad (ver enlace al principio de este documento).

Las asignaturas que se impartan durante el primer cuatrimestre podrán presentarse hasta el día 15 de noviembre de 2024, y las que se impartan durante el segundo cuatrimestre, hasta el 21 de febrero de 2025.

### Resolución de dudas

Para resolver dudas relacionadas con esta convocatoria, se ha planificado un videoencuentro que quedará grabado y colgado el enlace en esta misma web:

* Lunes 4 de septiembre, 11.00. Enlace al principio de este documento.

También, se pueden consultar las preguntas más frecuentes, con sus respuestas, más adelante en este documento. En caso de que no se puedan resolver así, se podrán plantear por correo electrónico a la dirección ofilibre@urjc.es, donde se les tratará de dar solución lo antes posible.

### Lista de comprobaciones de material depositado en BURJC Digital

Para todos los materiales que se depositen en BURJC Digital con idea de presentarlos a esta convocatoria, puede seguirse esta lista de comprobaciones, para asegurarse de que se cumple lo indicado en la convocatoria, y las buenas prácticas recomendables para materiales docentes en acceso abierto:

* Portada:

  * Título del material: ¿Se ha incluido un título del material en la portada? Normalmente, este título hará referencia al nombre de la asignatura, y a la tipología del material. Por ejemplo, "Apuntes para el alumno de Diseño de Aplicaciones Telemáticas" o "Transparencias de la asignatura Arte en la Edad Media".
  * Autores y/o autoras: ¿Se indica claramente el nombre completo de las personas que se consideran autoras de la obra?
  * Grado(s) en los que se imparte: ¿Se ha incluido el listado de grados (y en su caso, nombres específicos de la asignatura en esos grados) en que se imparte? Este listado se puede incluir en un tamaño de letra menor que el título, y no necesariamente debajo de este, pero debería dar una idea rápida a quien vea la portada de en qué asignaturas y grados se están usando el material en cuestión.
  * Fecha o año: ¿Se incluye una fecha, o al menos un año de la obra? Esto es importante para que el lector pueda ver rápidamente cuánto de actualizado puede estar el material, y sobre todo referirse de forma adecuada a la versión del material que le interese.
  * Logo de la licencia elegida: ¿Se incluye el logo de la licencia elegida en la portada? Es importante que el logo de la licencia elegida (Creative Commons Atribución o Creative Commons Atribución-CompartirIgual) aparezca en portada, si es preciso en tamaño pequeño, para que el lector tenga una idea rápida de que el documento se distribuye en acceso abierto.

* En página interior, normalmente la primera página impresa tras la portada:

  * Nota de copyright: "¿Se ha incluido una nota de copyright completa? Ha de incluirse una nota de copyright, incluyendo la palabra o el símbolo "Copyright", el año (o el intervalo de años) y el listado completo de las personas y/o organizaciones que detentan los derechos de autor de la obra.
  * Referencia a la licencia: ¿Se ha incluido una referencia precisa a la licencia utilizada? Debe incluirse una referencia detallada a la licencia concreta que se ha empleado  (Creative Commons Atribución o Creative Commons Atribución-CompartirIgual). Además, esta licencia debe ser la misma que se indicó, incluyendo su logo, en la portada.
  * Ejemplo de texto completo, incluyendo nota de copyright y referencia a la licencia:

```
©2024 Autora Mengánez Zutánez  
Algunos derechos reservados  
Este documento se distribuye bajo la licencia  
“Atribución-CompartirIgual 4.0 Internacional” de Creative Commons,
disponible en  
https://creativecommons.org/licenses/by-sa/4.0/deed.es
```

* En metadatos del depósito:

  * Mismos datos que en la portada del documento: ¿Coinciden los datos de la portada con los indicados en los formularios de depósito? En particular, es importante que el título, el listado de personas autoras y la licencia se correspondan exactamente con lo indicado en la portada de la obra.
  
Pueden verse más detalles sobre cómo licenciar y marcar la licencia de la obra en el documento [Publicación de materiales docentes en abierto](materiales-docentes-abierto).

### Preguntas más frecuentes sobre esta convocatoria[🔗](#faq)

#### Si ya me he presentado en convocatorias anteriores, ¿puedo participar en esta con la misma asignatura?

Sí, pero solo se valorarán las categorías en donde no se haya obtenido el máximo de puntos, y los incentivos tendrán que ver con el incremento en puntuación de la asignatura.

#### ¿Hay algún formato específico para los documentos (presentaciones, documentos de texto…)?

Aunque no es obligatorio, en la OfiLibre se han elaborado unas [plantillas que pueden ser utilizadas para los materiales publicados en acceso abierto](/guias/plantillas-asignaturas-abierto/).

#### ¿Cómo especifico la licencia en mis documentos?

Dependiendo del tipo de documento se suele incluir la licencia o bien al principio, o al final. En documentos de texto, suele aparecer al principio donde normalmente iría la nota de copyright. En vídeos, puede aparecer al principio, donde aparezca el título, o al final donde aparecerían los títulos de crédito. En locuciones de audio, igualmente al principio o al final. Lo importante es que aparezca dentro del material de forma que se pueda reconocer la licencia. Puede ver más información en [esta guía de publicación de materiales docentes en abierto](/guias/materiales-docentes-abierto/#marcado-con-la-licencia-elegida) donde se explica con más detalle cómo incluir la licencia en los materiales.

Este es un ejemplo: 

![](/images/cc-by-sa-miniatura.jpg)

```
©2024 Autora Hermenegilda Gómez Stravinsky

Algunos derechos reservados 

Este documento se distribuye bajo la licencia  

“Atribución-CompartirIgual 4.0 Internacional” de Creative Commons,

disponible en  

https://creativecommons.org/licenses/by-sa/4.0/deed.es

Esta licencia no se aplica a materiales de terceros que puedan estar incluidos en esta obra y que mantiene los derechos de los autores originales.
```

#### ¿Cómo se verá mi asignatura en abierto?

[Aquí puedes ver ejemplos](https://online.urjc.es/es/para-futuros-estudiantes/asignaturas-en-abierto) de asignaturas publicadas en la convocatoria anterior

#### ¿Cómo especifico la licencia en mis documentos?

Son dos las licencias que han sido aprobadas por el Consejo de Publicación Abierta de la URJC como licencias válidas para materiales en abierto:

* Creative Commons BY (Atribución) 4.0
* Creative Commons BY-SA (Atribución-CompartirIgual) 4.0.

En la [Guía sobre la publicación de materiales docentes en acceso abierto](/guias/materiales-docentes-abierto/) se explican en más detalle ambas licencias.

Dependiendo del tipo de documento se suele incluir la licencia o bien al principio, o al final. En documentos de texto, suele aparecer al principio donde normalmente iría la nota de copyright. En vídeos, puede aparecer al principio, donde aparezca el título, o al final donde aparecerían los títulos de crédito. En locuciones de audio, igualmente al principio o al final. Lo importante es que aparezca dentro del material de forma que se pueda reconocer la licencia. 

#### ¿Quién puede subir los materiales de una asignatura al archivo abierto?

Los materiales puede subirlos cualquiera de los autores, previa firma y envío de un documento de autorización de publicación en abierto firmado por el autor que sube los materiales que sirve como declaración de conformidad.

#### Una vez que los materiales tienen la licencia, ¿cómo se suben al archivo abierto?

Las instrucciones para subir los materiales al archivo abierto institucional pueden encontrarse en [esta página de la BURJC](https://burjcdigital.urjc.es/page/howtopublish).

#### ¿Quién puede presentar la solicitud para mi asignatura en la convocatoria?

Exclusivamente el responsable del grupo de actas de la asignatura, si es una sola la que se presenta, o de una cualquiera del grupo de asignaturas, si es un grupo de asignaturas con los mismos materiales lo que se presenta.

#### ¿Qué ocurre si mis materiales no encajan en ninguna de las categorías contempladas en la convocatoria?

En caso de materiales que no encajan en ninguna categoría, inclúyalos en otros y justifíquelo adecuadamente.

#### ¿Dónde pongo mis recursos abiertos en mi asignatura de aula virtual?

No hace falta ponerlos en ningún sitio en especial, solo es necesario incluir los enlaces en el formulario de participación.

#### ¿Cómo subo mis vídeos?

Los vídeos y podcasts se deben subir primero a la plataforma [TV URJC](https://tv.urjc.es). Conviene que consultes el [Procedimiento de publicación de materiales en abierto en TV URJC](https://infotic.urjc.es/pages/viewpage.action?pageId=154370093). En este procedimiento se indica cómo los vídeos se deben publicar en TV URJC en una misma colección, que será la que se indique posteriormente en el formulario de solicitud de esta convocatoria. También el procedimiento muestra cómo insertar en Aula Virtual los vídeos subidos a TV URJC.

Los vídeos subidos desde TV URJC a Aula Virtual siguiendo este procedimiento son visibles para los estudiantes de la asignatura. Si la asignatura se aprueba para que esté disponible en abierto, esa colección se abrirá al público en general, por lo que la podrá ver todo el mundo.

#### Ya he enviado la solicitud, pero quiero realizar cambios, ¿cómo lo hago?

Una vez enviada la solicitud no se puede modificar. Puede realizar otra solicitud, y el comité evaluador considerará siempre la última versión enviada para cada asignatura.

#### ¿Puedo aportar software (programas de ordenador) que utilizo en mis clases en alguna categoría?

El software libre que se utiliza en docencia podría aportarse en la categoría “Otros materiales” de la convocatoria, si cumple ciertos requisitos. Para explicar cuáles son estos requisitos, y en general para su consulta por los docentes que son autores de software libre que utilizan en sus clases hemos preparado unas recomendaciones específicas: [Convocatoria de asignaturas en abierto: software para docencia](/guias/convocatoria-asignaturas-abierto-software).

#### ¿Tengo que cambiar la forma de presentar mis materiales para participar?

No necesariamente. Aunque para subirlos al Archivo Abierto Institucional de la URJC (BURJC Digital) debe estar agrupados. La idea es mantener la forma en que damos las clases y hacer un compilado para depositar.

#### ¿Solo se pueden usar las licencias CC Atribución y CC Atribución-CompartirIgual? ¿Por qué no otras?

Las licencias aceptadas son CC Atribución y CC Atribución-CompartirIgual. No se aceptan otras porque no han sido aceptadas como licencias de publicación en acceso abierto por el Consejo de Publicación en Abierto, siguiendo la Definición de Berlín de Acceso Abierto.

#### ¿Cuándo es el último día para participar de la Convocatoria?

Pueden verse las fechas al comienzo de este documento. Pero atención, para poder enviar la solicitud, los materiales deben estar subidos, previamente, en los repositorios correspondientes. Es importante tener en cuenta que el Archivo Abierto Institucional de la URJC (BURJC Digital) debe poder procesar el material, aceptarlo y darle un handle (identificador url). Por lo tanto, la fecha límite de subida de material es dos días anterior a la fecha límite de la convocatoria. Si el material es rechazado por la biblioteca, no se obtendrá el handle y por tanto no se podrá participar en ese punto.

En la convocatoria, el punto 2 dice lo siguiente:

 “Han de incluir claramente en el encabezado, primera página o equivalente los siguientes datos: Autores, título, y fecha. También incluirá una referencia a la URJC, a la asignatura o asignaturas en las que se utiliza el material y al lugar de depósito (TV URJC o BURJC digital, incluyendo su enlace) y de forma clara la licencia de distribución en acceso abierto”. 

#### Entonces, cuando dice “incluyendo su enlace” ¿qué quiere decir?

De este trámite se encargará el mismo Archivo digital. Cuando se proceda a la inclusión en el archivo y se genere el enlace (handle) desde el mismo archivo incluirán esta información en el material depositado.

#### Quiero publicar un paquete de ejercicios, pero muchas veces son heredados de años y años… ¿puedo usarlos y subirlos al repositorio?

La convocatoria está pensada para liberar y publicar materiales de creación propia, o en coautoría con autorización de los restantes autores. Publicar algo como propio, sin estar seguro de si somos los autores o no, no es aconsejable.

#### Hasta el año pasado éramos dos profesoras impartiendo la asignatura. Para este curso solo la impartirá la otra profesora. ¿Puedo participar de la convocatoria?

No. Participan de la convocatoria los docentes que la imparten este año. Por otro lado, los materiales (que pueden ser en coautoría con otros profesores) si finalmente reciben reconocimiento económico, será para los profesores que imparten la asignatura en el actual curso.

#### Si tengo mis vídeos en Youtube, ¿sirve para la convocatoria?

No. Deben estar en el repositorio de URJC-TV con una de las licencias propuestas.

#### Tengo un programa de ordenador que me gustaría que participara de la convocatoria. ¿Cómo lo incluyo?

Los programas de ordenador sí pueden participar de la convocatoria. Hay un procedimiento para su almacenamiento y en el formulario de participación, debe incluirse en la categoría de “Programas de ordenador”

#### Si imparto clases en el segundo cuatrimestre… ¿Puedo participar ahora?

Sí, la convocatoria del segundo cuatrimestre queda abierta a la vez que la del primero, aunque cerrará en su propia fecha.

#### He subido vídeos a TV URJC y no los veo disponibles

Es normal, porque los videos están subidos, pero no son aún públicos. Lo serán si tu asignatura es finalmente puesta en abierto. Mientras tanto, sólo los alumnos de tu asignatura podrán verlos, llegando a ellos por enlace del aula virtual.

#### ¿Tengo Apuntes y Transparencias, ¿cómo las deposito?

Como son dos documentos PDF (correspondientes cada uno a una categoría) tendrían que ser dos entradas en BURJC Digital, una para cada documento.

#### Al subir mis apuntes a la BURJC… ¿los subo por tema o por tipo de material?

En esta convocatoria hay que subir los documentos en un único PDF por categoría. Eso no quita que en tus clases los utilices tema por tema, y tengas un PDF por tema (o por sesión de clase, o por lo que te parezca mejor). Pero en el momento de subirlo a BURJC Digital, ha de ser un solo documento. Luego, en alguna parte visible de la asignatura enlazarás a ese documento (puede ser, por ejemplo, “colección completa de apuntes”), para que cuando la asignatura se ponga en abierto se pueda ver claramente dónde está el documento completo.

El documento completo puede ser simplemente una concatenación de los PDF (si necesitas ayuda para componer esa concatenación, dinos), si es posible con una portada y un índice. O, dependiendo de la herramienta que uses para escribirlos, todos los apuntes en un documento conjunto, del que producirás un único PDF. Pero luego, si prefieres ir subiendo los materiales tema a tema para tus alumnos, porque así los manejáis mejor (o porque en ello haces pequeños cambios por ejemplo para arreglar errores que detectas al preparar las sesiones de cada semana), no hay ningún problema en ello. Únicamente, advierte a tus alumnos que mejor que usen las versiones tema a tema.

#### El máximo de incentivo económico personal ¿es para todo el curso?

El máximo que especifica la convocatoria es para todo el curso académico. Esto supone, por ejemplo, que si ya se ha alcanzado el máximo personal con asignaturas del primer cuatrimestre no se va a poder recibir incentivo económico si se participa en asignaturas presentadas en el segundo cuatrimestre. En cualquier caso, si se llega a este máximo, los demás beneficios de la convocatoria se seguirán aplicando, por lo que se puede participar en la convocatoria con asignaturas del segundo cuatrimestre aunque ya se haya alcanzado el máximo personal durante el primer cuatrimestre.

#### Una asignatura anual, ¿en qué cuatrimestre se puede presentar?

Puedes presentar la asignatura en la convocatoria de cualquiera de los dos cuatrimestres, pero sólo en una de ellas. En cualquier caso, se presentará toda la asignatura.

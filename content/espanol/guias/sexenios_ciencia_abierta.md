---
title: Sexenios y Ciencia Abierta
logo: logo-ofilibre.png
date: 2024-01-17
type: guias

bg_image: "sexenios_ciencia_abierta/sexenios.png"
feature: "sexenios_ciencia_abierta/sexenios.png"
---

Información relacionada con los aspectos de ciencia abierta de la Convocatoria de Sexenios 2023.

## Sesión informativa

Cuándo: Lunes 22 de enero de 2024, 13:00.

[Grabación de la sesión](https://tv.urjc.es/video/65b15438e8dbc093a310d0fb)

Temas que se trataron:

* Depósito de aportaciones en el Archivo Abierto de la Universidad
  - Depósito de artículos ya publicados en acceso abierto
  - Depósito de artículos con periodo de embargo
  - Depósito de artículos que no pueden publicarse en acceso abierto
  - Depósito de datos y software

* Mecánica de subida de artículos a BURJC Digital
  - Detalles sobre los formularios de subida de documentos al Archivo Abierto de la Universidad
  - Recomendaciones para subida de artículos que se vayan a usar como aportaciones en la Convocatoria de Sexenios

* Biliometría narrativa
  - Qué es la bibliometría narrativa
  - Cómo aplicar la bibliometría narrativa a nuestras aportaciones en la Convocatoria de Sexenios


## Información sobre la Convocatoria de Sexenios

[Información sobre la Convocatoria de Sexenios en el sitio web de ANECA](https://www.aneca.es/convocatoria-2023).

[Texto completo de la convocatoria](https://www.boe.es/boe/dias/2023/12/22/pdfs/BOE-A-2023-26094.pdf)

[Criterios específicos de la convocatoria](https://www.boe.es/boe/dias/2023/12/16/pdfs/BOE-A-2023-25537.pdf), por resolución de 5 de diciembre de 2023 de la CNEAI.

Baremos de las comisiones de evaluación:

- [Baremo general de evaluación de sexenios](https://www.aneca.es/documents/20123/48878/BaremosCamposSexenios.pdf/65570c01-034f-8bc9-d03d-09027743f73c?t=1704190907088)
- [Baremos específicos por campo](https://www.aneca.es/web/guest/orientaciones-baremos-de-los-criterios-de-la-evaluaci%C3%B3n-de-sexenios-en-la-convocatoria-2023)

Archivo institucional de la URJC:

* [BURJC Digital](https://burjcdigital.urjc.es)
* [Página para subir documentos a BURJC Digital](https://burjcdigital.urjc.es/submit)

Documentos de ayuda  preparadas por Biblioteca de la URJC:

* [Recursos para encontrar indicios de calidad en mis publicaciones en Acreditaciones y Sexenios](/images/guias/sexenios_ciencia_abierta/Recursos_Indicios_Sexenios.pdf) (presentación)
* [Nuevos criterios de evaluación de la Convocatoria 2023 de Sexenios de Investigación (documento de ayuda para el PDI de la URJC)](/images/guias/sexenios_ciencia_abierta/DOCUMENTOAYUDA_PDI_INFORME_21122023.pdf)
* [Sexenios 2023, principales cambios](/images/guias/sexenios_ciencia_abierta/SEXENIOS_2023.pdf) (infografía)
* [Cómo añadir aportaciones a BURJC Digital](/images/guias/sexenios_ciencia_abierta/COMO_ANADIR_OBRAS_A_BURJC_DIGITAL_2.pdf) (infografía)
* [Cinco pasos para subir una publicación a BURJC Digital](/images/guias/sexenios_ciencia_abierta/5PASOS_INFOGRAFIA.pdf) (infografía)
* [¿Qué metadatos de mi publicación necesito subir al repositorio?](/images/guias/sexenios_ciencia_abierta/METADATOS_INFOGRAFIA.pdf) (infografía)

Otros documentos e información de ayuda:

* [Recomendaciones de REBUIN para depósito de aportaciones](/images/guias/sexenios_ciencia_abierta/Recomendaciones_REBUIN.png)
* [Artículo de Manuel Gertrudix sobre sexenios](https://ciberimaginario.es/2023/12/07/sexenios-2023-recursos-para-hacer-la-bibliometria-narrativa/)

* Bibliometría Narrativa: aplicaciones para la defensa de currículos y aportaciones científicas en el marco de CoARA y ANECA - Sexenios, con Daniel Torres-Salinas y Wenceslao Arroyo-Machado (Departamento de Información y Comunicación, Universidad de Granada)
  * [Video](https://youtu.be/nuaOz2duw2w)
  * [Presentación](https://zenodo.org/records/10089106)

* [Resumen de la Universidad de Málaga sobre bibliometría narrativa y sexenios](https://biblioguias.uma.es/c.php?g=676474&p=5178748)

## Aspectos de la convocatoria relacionados con ciencia abierta

* Apartados fundamentales de los documentos relacionados con la convocatoria:

  * Criterios específicos:

    * Apartado 2 (indicios de relevancia e impacto, y narrativa asociada)
    * Apartado 5 (depósito en archivos)
    * Apéndice (criterios mínimos de los medios de difusión de publicaciones, ejemplos de criterios cuantitativos y métricas)

  * Baremo general (impacto científico incluyendo lista de indicios, impacto social, contribución a la ciencia abierta)
  * Baremos específicos por campo (especificación del baremo general para cada campo de conocimiento)
  
* Requisitos de depósito de las contribuciones:

Según los criterios específicos de la convocatoria (apartado 5):

> "se requerirá el depósito de los resultados de la investigación que se sometan a evaluación en repositorios institucionales, temáticos o generalistas de acceso abierto, incluyendo un identificador persistente (DOI, Handle, ARK, SWHID, o, en general, una URI/URL única permanente)."

> "En el caso de las publicaciones académicas, sean en formato artículo, libro o capítulo de libro, las personas solicitantes deberán aportar evidencia de haber depositado una copia de la versión final de la aportación aceptada para publicación en un repositorio de su institución o en un repositorio temático o generalista de acceso abierto."

> "El depósito podrá hacerse en acceso abierto, acceso restringido, embargado o con acceso solo a los metadatos, respetando en todos los casos la gestión de derechos de autoría amparada por el marco legal vigente en el momento de la publicación."

* Requisitos de depósito de conjuntos de datos que se sometan a evaluación:

Según los criterios específicos de la convocatoria (apartado 5):

> "Los conjuntos de datos que se sometan a evaluación deberán cumplir con los principios FAIR (fáciles de encontrar, accesibles, interoperables y reutilizables) y, siempre que sea posible, se difundirán en acceso abierto en repositorios o infraestructuras de datos de confianza."

* Requisitos de depósito de programas de ordenador que se sometan a evaluación:

Según los criterios específicos de la convocatoria (apartado 5):

> "en el caso de los programas de ordenador, se valorarán las contribuciones relevantes a programas distribuidos como software libre, entendiendo como tal aquel que cumpla la definición publicada por la Open Source Initiative ( https://opensource.org/osd/ ) y que, por tanto, esté protegido por alguna de las licencias aprobadas por este organismo ( https://opensource.org/licences/ )."

* ¿Dónde hay que tener depositadas las contribuciones?

Según las "Preguntas frecuentes (versión 0, de 16 de enero de 2024)" citadas en el sitio web de la convocatoria:

> "Un repositorio “válido” es aquel mantenido consistentemente por una institución, p. ej. una Universidad (repositorio institucional) o por una comunidad disciplinar establecida o institución con perfil disciplinar que tienen una garantía de perdurabilidad y mantenimiento en el tiempo y que cumplen con los estándares internacionales para la agregación de los materiales depositados en él. Se puede consultar la lista de repositorios institucionales nacionales en Recolecta."

> "Son ejemplos de repositorios temáticos arXiv en el ámbito de las Matemáticas, Física, Informática y Biología cualitativa, RePec en Economía, o CiteSeerX en Informática y ciencias de la información. Un ejemplo de repositorio generalista es Zenodo (que puede ser usado en caso de que la propia Universidad o Centro de investigación no cuente con un repositorio institucional)."

* ¿Qué repositorios no son válidos?

> "No son repositorios válidos, a los efectos de esta convocatoria de sexenios, las aplicaciones o sitios web con posibilidad de almacenamiento y difusión sin cumplir estándares de interoperabilidad y/o funciones de preservación, o que dependen directamente de agentes editoriales. Por ejemplo, no son repositorios las bases de datos bibliográficas, los catálogos bibliográficos, las redes sociales científicas (ej. ResearchGate, Academia.edu). Tampoco son repositorios válidos, a los efectos de esta convocatoria de sexenios, las plataformas de agregación de contenidos (por ejemplo, Dialnet)."

* ¿Qué se ha de aportar como "indicios de relevancia e impacto"?

Según los criterios específicos de esta convocatoria (apartado 2):

> "En el proceso de evaluación se aplicarán criterios y metodologías de evaluación cualitativas y cuantitativas. Para ello, se tomará como referencia la narrativa aportada por la persona solicitante en los «indicios de relevancia e impacto» de cada aportación. En este apartado de la solicitud se defenderá el impacto científico de la aportación, por ejemplo, a través de citas recibidas contextualizadas excluyendo autocitas, de su proyección nacional e internacional, de los proyectos nacionales o internacionales que han financiado la investigación o que se han derivado de ella, del cumplimiento de estándares de ética e integridad en la investigación, de los premios recibidos, de las traducciones de la obra, entre otros; y/o la contribución de dicha aportación a la generación de impacto social evidenciado, por ejemplo, a través de aportaciones al diseño e implementación de políticas públicas, contribución al desarrollo de soluciones a problemas sociales, o cualquier otro aspecto que se considere relevante. En la narrativa aportada se hará un uso responsable de indicadores cuantitativos (indicadores bibliométricos normalizados, entre otros)."

* ¿Qué ha de cumplir, como mínimo, cualquier medio de difusión para que pueda ser considerada una aportación publicada en él?

Los criterios específicos indican en su apéndice, los "criterios mínimos de los medios de difusión de publicaciones". En particular, su apartado III lista los criterios mínimos que ha de tener un medio de difusión (en caso de que no se cumplan los criterios de la tabla 1), y en la tabla 1 las *"posibles métricas, fuente y dimensiones para evidenciar los indicios de relevancia e impacto de las aportaciones presentadas a evaluación"*.

* ¿Deben depositarse en repositorio las publicaciones que ya están en acceso abierto?

Según las "Preguntas frecuentes (versión 0, de 16 de enero de 2024)" citadas en el sitio web de la convocatoria:

> "Sí. La acción de publicar es distinta a la acción de depositar y la publicación en abierto en una editorial científica no exime el depósito de la misma en un repositorio institucional o temático"

* Depósito de libros, capítulos de libros en papel, o con derechos parcial o totalmente cedidos a editoriales.

Según las "Preguntas frecuentes (versión 0, de 16 de enero de 2024)" citadas en el sitio web de la convocatoria:

> "El depósito no necesariamente implica la difusión en acceso abierto. Esta convocatoria de sexenios limita su exigencia a que los trabajos a evaluar se depositen, como indica la Ley, aunque este depósito se haga con el contenido embargado, con acceso restringido o completamente cerrado. El atributo “de acceso abierto” se refiere exclusivamente a los repositorios, no a los trabajos depositados en ellos."

> "En el caso de obras protegidas, embargadas o cuyos derechos de propiedad intelectual hayan sido cedidos a terceros, se debe hacer depósito, agregando los metadatos, en repositorios abiertos y, en la medida de lo posible, el texto completo, aunque la disponibilidad de este en abierto no sea efectiva hasta que se hayan aclarado los derechos o finalizado el embargo. Para obras cuyos derechos de propiedad intelectual hayan sido cedidos a perpetuidad (por ejemplo, libros), solo será necesario depositar los metadatos en el repositorio."

* ¿Cada coautor ha de hacer un depósito diferente?

Según las "Preguntas frecuentes (versión 0, de 16 de enero de 2024)" citadas en el sitio web de la convocatoria:

> "No, bastará con indicar el identificador persistente de dicho depósito (DOI, Handle o similar). Se ha de seguir el mismo procedimiento en caso de que el editor haya hecho el depósito en un repositorio."

* Depósito de artículos anteriores a 2011 (Ley de la Ciencia) y libros y capítulos de libros anteriores a 2022 (modificación de la Ley de la Ciencia)

Según las "Preguntas frecuentes (versión 0, de 16 de enero de 2024)" citadas en el sitio web de la convocatoria, este depósito no es preciso, pero sí recomendable:

> "Más allá de las obligaciones derivadas de la Ley de la Ciencia, la Tecnología y la Innovación (de 2011 y su posterior modificación de 2022), se recomienda a todas las personas solicitantes avanzar en el depósito de sus aportaciones (incluidas las anteriores al 1 de junio de 2011), independientemente de su fecha de publicación, para garantizar la preservación de los trabajos y facilitar su difusión."


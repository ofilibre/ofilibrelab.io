---
title: Migración de un proyecto Hugo desde GitLab Pages a Netlify
logo: netlify-logo.png
date: 2025-02-27
type: guias
---

En esta guía explicaremos cómo desplegar en Netlify un sitio estático desarrollado con Hugo, cuyo código se encuentra alojado en un repositorio GitLab y que actualmente está desplegado en GitLab Pages.  

0\. Instalamos el **material preliminar**. En nuestro caso necesitaremos el editor de código Visual Studio Code (VS Code), el software de control de versiones Git y Hugo.  

1\. Realizamos una **copia** (fork) del repositorio actual y lo clonamos para llevarlo a nuestro **equipo local**. Posteriormente, abrimos el directorio raíz en VS Code para hacer todos los cambios que se describen en los pasos siguientes.  

2\. **Creamos el archivo netlify.toml** en la raíz del proyecto, teniendo en cuenta nuestra versión de Hugo, el nombre de la carpeta en la que se guardarán los archivos generados por Hugo (_publish_) y el comando que usará Netlify para desplegar el sitio (_command_). En nuestro el archivo sería:  

```
\[build.environment\]  
HUGO\_VERSION = "0.123.7"  
\[build\]  
publish = "public"  
command = "hugo"
```

3\. **Modificamos el archivo static/admin/index.html**, eliminando su contenido y copiando el siguiente:  

```

<!DOCTYPE html>

<html lang="en">  

  <head>

    <meta charset="UTF-8" />

    <title>Content Manager</title>

    <!-- the script for authentication using Netlify Identity -->

    <script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>

  </head>

  <body>

    <!-- the script that builds the page and powers Netlify CMS -->

    <script src="https://unpkg.com/netlify-cms@^2.0.0/dist/netlify-cms.js"></script>

  </body>

</html>

```


Este código es el punto de entrada para Netlify CMS (un sistema de gestión de contenidos de código abierto para editar sitios estáticos ofreciendo a los editores una interfaz de usuario amigable y flujos de trabajo Git) en nuestro sitio. Concretamente, este archivo habilita Netlify Identity, el sistema de autenticación que se usará para acceder a Netlify CMS, y posteriormente carga Netlify CMS desde la CDN de [unpkg](https://unpkg.com/). Una vez autenticado, el usuario puede crear, editar y publicar contenido dentro del CMS.   

4\. **Modificamos la sección _backend_ del archivo static/admin/config.yml**, cambiando  

```
backend:  
  name: gitlab  
  repo: ofilibre/ofilibre.gitlab.io  
  branch: master # Branch to update (optional; defaults to master)  
  api\_root: https://gitlab.com/api/v4   
  base\_url: https://gitlab.com  
  auth\_type: pkce  
  app\_id: ba44978a94e18763a29c4f3f365d09e9d6e5b4fdbbb02c271ba13d0cbf0694d5  
  auth\_endpoint: oauth/authorize    
```

por  

```
backend:  
  name: git-gateway  
  branch: master
```

Esta sección define la configuración del backend, es decir, cómo y dónde se almacenan los cambios cuando los usuarios editan contenido en Netlify CMS. En cuanto a los campos _name_ y _branch_, en el paso 8 se explicará su papel. 

5\. **Desplegamos el Proyecto en Netlify**. Una vez tenemos la sesión iniciada en Netlify seguimos los siguientes pasos:  

*  Seleccionamos "Add New Site" y luego "Import an existing project".  
    
*  En el selector del proveedor de Git, elegimos GitLab.  
    
*  Seleccionamos el repositorio y el Team adecuado.  
    
*  Configuramos el sitename como `Ofilibre.`  
    
*  Seleccionamos la rama correcta (en nuestro caso, master).

6\. **Modificamos el archivo config.toml**. Volviendo al proyecto, accedemos al archivo config.toml (ubicado en la raíz) y cambiamos el valor de baseURL a la URL proporcionada por Netlify (en base al _sitename_). En nuestro caso:  

baseURL = "https://ofilibre.netlify.app"  

Guardamos los cambios y actualizamos el repositorio (_push_). Una vez los cambios se ven reflejados (aproximadamente dos minutos) ya debemos de poder acceder a nuestra página web usando la URL https://ofilibre.netlify.app.  

7\. **Configuramos Netlify Identity.** Una vez hemos accedido al panel de administración del sitio en Netlify, nos dirigimos a la sección de Identity y seleccionamos "Enable Identity" para activar Netlify Identity. Una vez habilitado, configuramos las siguientes opciones según las necesidades:  

*   Invite Users: Si necesitamos invitar a usuarios al sitio.  
    
*   External Providers: Si queremos autenticarnos mediante un proveedor de autenticación externo (como Google o Gitlab). En nuestro caso fue necesario utilizar un proveedor externo pues tuvimos errores para crear la cuenta de Netlify Identity, que es el método de autenticación por defecto.  
    
*   Enable Git Gateway: Habilitamos esta opción para permitir la integración con GitLab.  
    
8\. **Configuramos un Token de acceso.** Los Personal Access Tokens en GitLab son claves de autenticación utilizadas para interactuar con la API de GitLab o realizar operaciones Git sin necesidad de usar tu nombre de usuario y contraseña.  

Los tokens de Git-Gateway en Netlify son claves generadas para permitir que un usuario (generalmente a través de una interfaz como Netlify CMS) interactúe con un repositorio Git sin necesidad de acceder directamente a la API del proveedor Git, es decir, Git-Gateway actúa como un intermediario entre Netlify CMS y el proveedor Git. Cuando inicias sesión en Netlify CMS, Git-Gateway autentica al usuario y gestiona las solicitudes al repositorio Git en tu nombre en la rama (_branch_) indicada en la sección _backend_ (paso 4).  

La versión GitLab 15.0 eliminó el soporte para OAuth tokens sin fecha de expiración, lo que significa que cualquier token anterior (sin expiración) dejó de ser válido, y que los OAuth tokens pasan a tener una fecha de expiración de dos horas.  

Al habilitar Netlify Identity, se nos proporciona un OAuth token, por lo que a partir de las dos horas no podremos acceder a Netlify CMS. La solución es crear un Gitlab personal access token:  

*   Accedemos a nuestro perfil de GitLab, concretamente a la sección Edit profile.  
    
*   Seleccionamos la opción Access tokens y luego Add new token.

*   Configuramos el nombre, la descripción y la fecha de expiración del token.  
    
*   Configuramos los permisos del token, asegurando que las opciones api, read\_api, read\_repository y write\_repository estén habilitadas.  
    
*   Copia el token y ve a la configuración de Netlify.  
    
*   Pega el Personal Access Token de GitLab en la configuración de Netlify Identity para permitir la autenticación entre Netlify y GitLab.  
    
9\. **Probamos Netlify CMS.** La configuración actual permite la publicación y edición de blogs y guías.  

10\. **Modificamos el archivo static/admin/config.yml**, para permitir la edición y publicación de fichas y presentaciones, agregando en cada colección los campos correspondientes. Además, modificamos la gestión de archivos adjuntos a través de los atributos media\_folder y public\_folder. La sección _collections_ resultante sería:  

```
collections:
  - name: 'blog'
    label: 'Blog'
    folder: 'content/espanol/blog'
    create: true
    slug: '{{year}}-{{month}}-{{day}}-{{slug}}'
    media_folder: '/../../../static/images/blog/{{slug}}'
    public_folder: '/images/blog/{{slug}}'
    editor:
      preview: true
    fields:
      - { label: 'Título', name: 'title', widget: 'string' }
      - { label: 'Fecha de publicación', name: 'date', widget: 'date' }
      - { label: 'Descripción', name: 'description', widget: 'string' }
      - { label: 'Tipo', name: 'type', widget: 'hidden', default: 'post'}
      - { label: 'Categorías', name: 'categories', widget: 'hidden', default: ["OfiLibre"]}
      - { label: 'Etiquetas', name: 'tags', widget: 'list'}
      - { label: 'Fondo (alargado)', name: 'bg_image', widget: 'image', default: '/images/backgrounds/page-title.jpg'}
      - { label: 'Logo (cuadrado)', name: 'thumb', widget: 'image', default: '/images/logo-ofilibre.png'}
      - { label: 'Cuerpo', name: 'body', widget: 'markdown' }
      - label: 'Archivos adjuntos'
        name: 'files'
        widget: 'list'
        media_folder: '/../../../static/documentos/{{slug}}'
        public_folder: '/documentos/{{slug}}'
        fields:
          - { label: 'Archivo', name: 'file', widget: 'file' }

  - name: 'guias'
    label: 'Guías'
    folder: 'content/espanol/guias'
    create: true
    slug: '{{slug}}'
    media_folder: '/../../../static/images/guias/{{slug}}'
    public_folder: '../../images/guias/{{slug}}'
    editor:
      preview: true
    fields:
      - { label: 'Título', name: 'title', widget: 'string' }
      - { label: 'Fecha de publicación', name: 'date', widget: 'date' }
      - { label: 'Descripción', name: 'description', widget: 'string' }
      - { label: 'Logo', name: 'logo', widget: 'image' }
      - { label: 'Fondo', name: 'feature', widget: 'image', required: false }
      - { label: 'Categorías', name: 'categories', widget: 'hidden', default: ["OfiLibre"]}
      - { label: 'Etiquetas', name: 'tags', widget: 'list'}
      - { label: 'Tipo', name: 'type', widget: 'hidden', default: 'guias'}
      - { label: 'Cuerpo', name: 'body', widget: 'markdown' }
      - label: 'Archivos adjuntos'
        name: 'files'
        widget: 'list'
        media_folder: '/../../../static/documentos/{{slug}}'
        public_folder: '/documentos/{{slug}}'
        fields:
          - { label: 'Archivo', name: 'file', widget: 'file' }

  - name: 'fichas'
    label: 'Fichas'
    folder: 'content/espanol/fichas'
    create: true
    slug: '{{slug}}'
    media_folder: '/../../../static/images/fichas/{{slug}}'
    public_folder: ''
    editor:
      preview: true
    fields:
      - { label: 'Título', name: 'title', widget: 'string' }
      - { label: 'Fecha de publicación', name: 'date', widget: 'date' }
      - { label: 'Logo', name: 'logo', widget: 'image' }
      - { label: 'Fondo', name: 'feature', widget: 'image', required: false }
      - { label: 'Página web', name: 'website', widget: 'string' }
      - { label: 'Página web (español)', name: 'website_es', widget: 'string', required: false }
      - label: 'Licencias'
        name: 'licenses'
        widget: 'list'
        fields:
          - { label: 'Nombre', name: 'name', widget: 'string' }
          - { label: 'URL', name: 'url', widget: 'string' }
      - { label: 'MyApps', name: 'myapps', widget: 'boolean', default: false }
      - { label: 'Código fuente (URL)', name: 'source', widget: 'string' }
      - label: 'Guías de instalación'
        name: 'installs'
        widget: 'list'
        fields:
          - { label: 'Nombre', name: 'name', widget: 'string' }
          - { label: 'URL', name: 'url', widget: 'string' }
          - { label: 'Formato', name: 'kind', widget: 'string', required: false }
      - label: 'Tutoriales'
        name: 'tutorials'
        widget: 'list'
        fields:
          - { label: 'Nombre', name: 'name', widget: 'string' }
          - { label: 'URL', name: 'url', widget: 'string' }
          - { label: 'Formato', name: 'kind', widget: 'string', required: false }
      - label: 'Extras'
        name: 'others'
        widget: 'list'
        required: false
        fields:
          - { label: 'Nombre', name: 'name', widget: 'string' }
          - { label: 'URL', name: 'url', widget: 'string' }
      - label: 'Capturas de pantalla'
        name: 'screenshots'
        widget: 'list'
        fields:
          - { label: 'Nombre', name: 'name', widget: 'string' }
          - { label: 'Archivo', name: 'file', widget: 'image' }
          - { label: 'Atribución', name: 'attribution', widget: 'string', required: false }
      - { label: 'Tipo', name: 'type', widget: 'hidden', default: 'fichas' }
      - { label: 'Cuerpo', name: 'body', widget: 'markdown' }
      - label: 'Archivos adjuntos'
        name: 'files'
        widget: 'list'
        media_folder: '/../../../static/documentos/{{slug}}'
        public_folder: '/documentos/{{slug}}'
        fields:
          - { label: 'Archivo', name: 'file', widget: 'file' }

  - name: 'pres'
    label: 'Presentaciones'
    folder: 'content/espanol/pres'
    create: true
    slug: '{{slug}}'
    media_folder: '/../../../static/transpas/{{slug}}'
    public_folder: ''
    editor:
      preview: true
    fields:
      - { label: 'Título', name: 'title', widget: 'string' }
      - { label: 'Fecha de publicación', name: 'date', widget: 'date' }
      - { label: 'Descripción', name: 'description', widget: 'string' }
      - { label: 'Imagen destacada', name: 'feature', widget: 'image' }
      - { label: 'Imagen teaser', name: 'teaser', widget: 'image' }
      - label: 'Transparencias'
        name: 'transpas'
        widget: 'object'
        fields:
          - { label: 'Archivo PDF', name: 'pdf', widget: 'file' }
          - { label: 'Archivo ODP', name: 'odp', widget: 'file' }
      - label: 'Extras'
        name: 'extras'
        required: false
        widget: 'list'
        fields:
          - { label: 'Nombre', name: 'name', widget: 'string' }
          - { label: 'URL', name: 'url', widget: 'string' }
      - { label: 'Tipo', name: 'type', widget: 'hidden', default: 'pres' }
      - { label: 'Cuerpo', name: 'body', widget: 'markdown' }
```

11\. **Configuración de DNS y dominio**  

*  Agregamos el dominio personalizado en Netlify (Domain management -> Add a domain -> ofilibre.urjc.es), del cual la universidad es propietaria.  
    
*  En este paso elegimos si utilizamos Netlify DNS como proveedor de DNS o continuamos con nuestro proveedor actual. En nuestro caso hemos elegido la segunda opción, por lo que deberemos comunicar al proveedor los cambios a realizar en los registros DNS para que nuestro dominio apunte al sitio desplegado en Netlify.  
    
*  Siguiendo la documentación de Netlify, dado que queremos registrar un dominio principal (_apex domain_), le comunicamos a nuestro proveedor DNS que busque la configuración de registros DNS de ofilibre.urjc.es y agregue un registro ALIAS con el campo host igual a '@' y que apunte a `apex-loadbalancer.netlify.com.` Los cambios pueden tardar hasta 48 horas en verse reflejados.  
    
*  Solicitamos un certificado SSL a través de la interfaz de gestión de certificados Harica. Para ello, nos dirigimos a la sección 'Server' dentro de la interfaz y rellenamos los campos, como el FQDN (en nuestro caso ofilibre.urjc.es), tipo de certificado (SSL DV) y algoritmo (RSA 2048). Además, seleccionamos la opción de generar una clave privada.
*  Cuando el certificado sea aprobado, se nos proporcionará una serie de archivos, entre los cuales el propio certificado y la cadena de certificados intermedios. Utilizando estos dos archivos y la clave privada del paso anterior, agregamos el certificado a Netlify (Domain management -> HTTPS -> Set custom certificate) para habilitar https.  
    
12\. **Modificamos el archivo config.toml** **y comprobamos el cambio DNS**. Volviendo al proyecto, accedemos al archivo config.toml y cambiamos el valor de baseURL a la URL definitiva, es decir:  

baseURL = "https://ofilibre.urjc.es"  

Finalmente, comprobamos que el cambio en DNS se haya registrado correctamente limpiando la caché (sudo resolvectl flush-caches en Linux) y utilizando el comando dig (dig ofilibre.urjc.es) para ver que efectivamente, el dominio apunte a `apex-loadbalancer.netlify.com.`  
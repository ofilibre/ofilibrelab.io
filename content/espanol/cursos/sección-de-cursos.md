---
title: Sección de Cursos
date: 2025-02-06T16:01:09.796Z
type: cursos
adicional: ¡Arrancamos!
---
¡Bienvenido al apartado de cursos!

En esta nueva sección se encontrarán una variedad de cursos relacionados con la cultura y el software libre.
---
title: "Publicación Abierta en Investigación"
date: 2023-03-27
feature: publicacion-abierta-investigacion.png
teaser: publicacion-abierta-investigacion.png

transpas:
    pdf: Publicacion_Abierta_Investigacion.pdf
    odp: Publicacion_Abierta_Investigacion.odp

type: pres
---

Presentación sobre publicación abierta en investigación.

Otras versiones de esta presentación:

* Investigación doctoral y publicación en acceso abierto, Ciclo interuniversitario de formación doctoral, Universidades Comunidad de Madrid, 4 de febrero de 2025 ([formato PDF](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_EID_2025-02-04.pdf), [formato ODP para LibreOffice](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_EID_2025-02-04.odp)).

* Ciencia Abierta y Publicación Científica en Abierto, Plan de Formación de PDI, Universidad Rey Juan Carlos, 14 de enero de 2025 ([formato PDF](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_PDI_2025-01-13.pdf), [formato ODP para LibreOffice](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_PDI_2025-01-13.odp)).

* Investigación doctoral y publicación en acceso abierto, Ciclo interuniversitario de formación doctoral, Universidades Comunidad de Madrid, 13 de diciembre de 2023 ([formato PDF](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_EID_2023-12.pdf), [formato ODP para LibreOffice](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_EID_2023-12.odp)).

* Publicación abierta en investigación,  II Jornadas de Cultura Libre de la URJC, 29 de marzo de 2023 ([formato PDF](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_2023-03.pdf), [formato ODP para LibreOffice](/transpas/publicacion-abierta-investigacion/Publicacion_Abierta_Investigacion_EID_2023-03.odp)).

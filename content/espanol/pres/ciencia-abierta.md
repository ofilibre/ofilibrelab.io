---
title: Ciencia Abierta
date: 2022-05-26
feature: ciencia-abierta.png
teaser: ciencia-abierta.png

transpas:
    pdf: Ciencia_Abierta.pdf
    odp: Ciencia_Abierta.odp

type: pres
---

Presentación sobre ciencia abierta.

Presentaciones anteriores sobre este mismo tema:

* Presentación en el cuurso de formación para PDI de la URJC "Investigación Responsable: Ética y Buenas Prácticas Científicas" (febrero de 2025): [PDF](/transpas/ciencia-abierta/Ciencia_Abierta_Etica_2025-02-11.pdf), [ODP, para LibreOffice](/transpas/ciencia-abierta/Ciencia_Abierta_Etica_2025-02-11.odp)

* Presentación en la asignatura "Ética en la investigación", de la Escuela Internacional de Doctorado de la URJC (mayo de 2022): [PDF](/transpas/ciencia-abierta/Ciencia_Abierta-2022-05-26.pdf), [ODP, para LibreOffice](/transpas/ciencia-abierta/Ciencia_Abierta-2022-05-26.odp)